package com.alababic.pick2me.shop_details.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.shop_details.CategoryPageRVAdapter;
import com.alababic_app.pick2me_data.repositories.ProductRepository;
import com.github.florent37.materialviewpager.header.MaterialViewPagerHeaderDecorator;
import com.alababic_app.pick2me_data.models.ProductItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class CategoryRecyclerViewFragment extends Fragment {

    static final boolean GRID_LAYOUT = true;
    private static final int ITEM_COUNT = 10;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private List<Object> mContentItems = new ArrayList<>();

    public static CategoryRecyclerViewFragment newInstance() {
        return new CategoryRecyclerViewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager;

        if (GRID_LAYOUT) {
            if (Utils.isTablet(getActivity()))
                layoutManager = new GridLayoutManager(getActivity(), 6);
            else
                layoutManager = new GridLayoutManager(getActivity(), 3);
        } else {
            layoutManager = new LinearLayoutManager(getActivity());
        }

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        //Use this now
        mRecyclerView.addItemDecoration(new MaterialViewPagerHeaderDecorator());

        mAdapter = new CategoryPageRVAdapter(mContentItems);

        //mAdapter = new RecyclerViewMaterialAdapter();
        mRecyclerView.setAdapter(mAdapter);

        ProductRepository.sharedInstance.getProductData().subscribe(new Consumer<List<ProductItem>>() {
            @Override
            public void accept(List<ProductItem> productItems) throws Exception {
                mContentItems.addAll(productItems);
                mAdapter.notifyDataSetChanged();
            }
        });

    }
}
