package com.alababic.pick2me.shop_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.base.BaseActivity;
import com.alababic.pick2me.shop_details.fragment.CategoryRecyclerViewFragment;
import com.alababic.pick2me.shop_details.fragment.ScrollFragment;
import com.alababic.pick2me.utils.PicassoCache;
import com.alababic.pick2me.utils.commons.Constants;
import com.alababic_app.pick2me_data.models.Shop;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;

import de.hdodenhof.circleimageview.CircleImageView;

public class ShopDetailsActivity extends BaseActivity {

    private MaterialViewPager mViewPager;
    private Toolbar toolbar;


    public static void start(Context context, Shop shop) {
        Intent intent = new Intent(context, ShopDetailsActivity.class);
        intent.putExtra(Constants.SHOP_ITEM_ARG, shop);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categ_pages);

        setTitle("");

        mViewPager = (MaterialViewPager) findViewById(R.id.materialViewPager);

        mViewPager.getPagerTitleStrip().setTextColor(ContextCompat.getColor(this , android.R.color.white));

        toolbar = mViewPager.getToolbar();

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        Shop shop;

        if ((shop = getIntent().getExtras().getParcelable(Constants.SHOP_ITEM_ARG)) != null) {
            ((TextView) mViewPager.getHeaderBackgroundContainer().findViewById(R.id.shop_name_header)).setText(shop.getName());
            PicassoCache.getPicassoInstance(this).load(shop.getImageURL()).fit().centerCrop()
                    .into((CircleImageView) mViewPager.getHeaderBackgroundContainer().findViewById(R.id.shop_image_header));
        }

        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                switch (position % 4) {
                    case 0:
                        //mViewPager.setImageDrawable();
                        return ScrollFragment.newInstance();
                    case 1:
                        return CategoryRecyclerViewFragment.newInstance();
                    case 2:
                        return CategoryRecyclerViewFragment.newInstance();
                    default:
                        return CategoryRecyclerViewFragment.newInstance();
                }
            }

            @Override
            public int getCount() {
                return 4;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position % 4) {
                    case 0:
                        return "Details";
                    case 1:
                        return "Food";
                    case 2:
                        return "Clothes";
                    case 3:
                        return "Pharmacy";
                    case 4:
                        return "Spices";
                    case 5:
                        return "Fibre Crops";
                    case 6:
                        return "Oils and Seeds";
                }
                return "";
            }
        });

        mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
            @Override
            public HeaderDesign getHeaderDesign(int page) {
                switch (page) {
                    case 0:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.colorPrimary,
                                "");
                    case 1:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.colorPrimary,
                                "");
                    case 2:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.colorPrimary,
                                "");
                    case 3:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.colorPrimary,
                                "");
                }

                //execute others actions if needed (ex : modify your header logo)

                return null;
            }
        });

        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());

    }
}
