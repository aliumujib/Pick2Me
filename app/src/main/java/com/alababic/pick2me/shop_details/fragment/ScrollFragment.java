package com.alababic.pick2me.shop_details.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.utils.commons.Constants;
import com.alababic_app.pick2me_data.models.NamedLocation;
import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.alababic.pick2me.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * Created by florentchampigny on 24/04/15.
 */
public class ScrollFragment extends Fragment implements OnMapReadyCallback {

    private NestedScrollView mScrollView;
    private MapView mLiteMapView;

    private String TAG = getClass().getSimpleName();
    private GoogleMap mMap;

    public static ScrollFragment newInstance() {
        return new ScrollFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scroll, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mScrollView = (NestedScrollView) view.findViewById(R.id.scrollView);

        MaterialViewPagerHelper.registerScrollView(getActivity(), mScrollView);

        mLiteMapView = (MapView) view.findViewById(R.id.lite_map_view);
        initializeMapView();
    }

    public void initializeMapView() {
        if (mLiteMapView != null) {
            // Initialise the MapView
            mLiteMapView.onCreate(null);
            // Set the map ready callback to receive the GoogleMap object
            mLiteMapView.getMapAsync(this);
        }
    }

    private void setMapLocation(GoogleMap map, NamedLocation data) {
        // Add a marker for this item and set the camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(data.location, Constants.MAP_ZOOM));
        map.addMarker(new MarkerOptions().position(data.location));
        // Set the map type back to normal.
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        mMap = googleMap;
        NamedLocation data = new NamedLocation("LOCATION", new LatLng(2.8581776, -3.1683902));
        setMapLocation(mMap, data);
    }
}
