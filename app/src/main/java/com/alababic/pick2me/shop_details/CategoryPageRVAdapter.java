package com.alababic.pick2me.shop_details;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.productdetails.ProductDetailsActivity;
import com.alababic.pick2me.utils.Utils;
import com.alababic_app.pick2me_data.models.ProductItem;

import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class CategoryPageRVAdapter extends RecyclerView.Adapter<CategoryPageRVAdapter.ViewHolder> {

    List<Object> contents;

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;

    public CategoryPageRVAdapter(List<Object> contents) {
        this.contents = contents;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
         /*   case 0:
                return TYPE_HEADER;*/
            default:
                return TYPE_CELL;
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view_auto, parent, false);
        return new CategoryPageRVAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CategoryPageRVAdapter.ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {
        /*    case TYPE_HEADER:
                break;*/
            case TYPE_CELL:

                final ProductItem object = (ProductItem) contents.get(position); /*new ProductItem("https://static-market.jumia.com.ng/p/other-9925-3057221-1-zoom.jpg", "Large Vegetable Pack", "guegue123","Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                        "2kg","2000", "Large Vegetable Pack.\n" +
                        "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                        "They include\n" +
                        "1 kg Aubergine/Eggplant\n" +
                        "1 kg Beetroot\n" +
                        "Cabbage (Red)\n" +
                        "1 kg Carrot\n" +
                        "Cauliflower\n" +
                        "Bunch of Celery (local)\n" +
                        "1 kg Cucumber\n" +
                        "250g Ginger\n" +
                        "200g Kale\n" +
                        "Lettuce \n" +
                        "Bunch Leek/Spring Onions\n" +
                        "Mushroom pack\n" +
                        "500g Pumpkin/Zucchini\n" +
                        "100g Parsley/Mintleaf\n" +
                        "500g Radish/Broccoli\n" +
                        "200g Spinach\n" +
                        "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                        "Same-day, Next-day and Planned delivery available\n" +
                        "Some of these produce are affected by seasonality... \n" +
                        "You can also customize your order, contact");*/
                Utils.loadImage(viewHolder.imageView, object.getmImageUrl(), viewHolder.imageView.getContext());
                viewHolder.productNmae.setText(object.getmItemName());
                viewHolder.productSeller.setText(object.getmSellerName());
                viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProductDetailsActivity.start(viewHolder.imageView.getContext(), object);
                    }
                });
                break;
        }
    }

    public final class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView productNmae;
        protected TextView productSeller;

        public ViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.item_icon);
            this.productNmae = (TextView) view.findViewById(R.id.item_title);
            this.productSeller = (TextView) view.findViewById(R.id.item_seller);
        }
    }
}