package com.alababic.pick2me.utils.views.thumbnailpickerview;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;


import com.alababic.pick2me.R;
import com.alababic.pick2me.productdetails.interfaces.ProductDetailView;
import com.alababic.pick2me.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by abdulmujibaliu on 8/31/17.
 */

public class ThumbnailPickerView extends LinearLayout {

    private final View mView;
    private ProductDetailView mListener;
    private RecyclerView mAttachmentsRV;
    public static int INVALID_REQUEST_CODE_NUMBER = -2;
    protected FilesRecyclerAdapter mAttachmentsAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected List<String> mDataSource = new ArrayList<>();

    private String TAG = getClass().getSimpleName();
    private ProductDetailView onThumbNailsClickedListener;


    public ThumbnailPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);

////        TypedArray a = context.obtainStyledAttributes(attrs,
////                R.styleable.AttachmentPickerView, 0, 0);
//        String titleText = a.getString(R.styleable.AttachmentPickerView_picker_view_title);
//        String buttonText = a.getString(R.styleable.AttachmentPickerView_picker_button_text);
//        int requestCode = a.getInt(R.styleable.AttachmentPickerView_request_code, INVALID_REQUEST_CODE_NUMBER);
//
//
//        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = inflater.inflate(R.layout.thumbnails_view_layout, this, true);

        mAttachmentsRV = mView.findViewById(R.id.thumbnails);

        initAttachmentRV(mAttachmentsRV, mDataSource);

        if (getActivity() instanceof ProductDetailView) {
            mListener = (ProductDetailView) getActivity();
        } else {
            ToastUtils.shortToast("OMO YAWA DEY FOR HERE O: CHECK ATTACHMENT PICKER VIEW");
        }

    }

    public void setImages(List<String> imagesList) {
        mAttachmentsAdapter.addAll(imagesList);
    }

    public Activity getActivity() {
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

    protected void initAttachmentRV(RecyclerView recyclerView, List<String> dataSource) {
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false); //Kind of opimal for all screen sizes
        recyclerView.setLayoutManager(mLayoutManager);
        mAttachmentsAdapter = new FilesRecyclerAdapter(dataSource, getContext());
        recyclerView.setAdapter(mAttachmentsAdapter);
    }

    public void setOnThumbNailsClickedListener(ProductDetailView onThumbNailsClickedListener) {
        this.onThumbNailsClickedListener = onThumbNailsClickedListener;
        mAttachmentsAdapter.setThumbNailClickListener(onThumbNailsClickedListener);
    }
}
