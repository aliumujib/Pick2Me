package com.alababic.pick2me.utils.commons;


/**
 * Created by Abdul-Mujeeb Aliu on 4/14/2016.
 */
public class Constants {

    public static final String APP_ID = "com.alium.spacetraders";

    //Intent Extras
    public static final String PROFILE_EXTRA_LAUNCHER = "launcher_profile_extra";

    //
    public static final String PARSE_GENERAL_OBJ_ID_COLUMN = "objectId";


    //User Class contant
    public static final String USER_CLASS_ACCOUNT_TYPE = "account_type";
    public static final String USER_CLASS_PHONE_NUMBER = "phone_number";
    public static final String USER_CLASS_PROFILE_PIC = "profile_pic";
    public static final String USER_CLASS_BACKGROUND_PIC = "background_pic";
    public static final String USER_CLASS_USER_FNAME = "first_name";
    public static final String USER_CLASS_USER_LNAME = "last_name";
    public static final String USER_CLASS_USERNAME = "username";
    public static final String USER_TABLE_TRADER_GENERIC_TAGS_ARRAY = "product_tags";
    public static final String USER_HOME_LOCATION = "user_location";
    public static final String USER_CLASS_USER_EMAIL = "email";
    public static final String USER_CLASS_USER_HEADER = "header_text";
    public static final String USER_PROFILE = "public_profile";
    public static final String USER_FRIENDS = "user_friends";

    public static final String PARSE_CREATED_AT = "createdAt";
    public static final String ADD_IMAGE_PIC = "ADD NEW";
    public static final String USER_CLASS_TABLE_NAME = "_User";
    public static final int REQUEST_CAMERA_MAIN = 1244;
    public static final int REQUEST_CAMERA_PROFILE_PIC = 1;
    public static final String PARSE_USER_USERNAME = "username";
    public static final String PARSE_USER_LOCATION = "location";
    public static final String PARSE_USER_LOCATION_LAT_LNG = "user_location_lat_lng";
    public static final int WITHBSTATE = 3;
    public static final int WITHOUTBSTATE = 4;
    public static final int REQUEST_CAMERA_PROFILE_HEADER = 2;
    public static final String FOLLOW_USER_RELATION = "follow_relation";
    public static final String PROFILE_OBJ_ARG = "profile_obj_arg";
    public static final String APP_PHOTO_URI = "photo_uri_parcelable";
    public static final String POSTS_TABLE_NAME = "Posts";
    public static final String POSTS_TABLE_LOCATION_GEOPOINT = "location";
    public static final String POSTS_TABLE_CAPTION = "caption_column";
    public static final String POSTS_TABLE_IMAGE_PARSE_FILE = "image_file";
    public static final String POSTS_TABLE_POSTER_OBJ = "parse_user_obj";
    public static final String RECOURSE_ITEM_EXTRA = "recourse_item";
    public static final String AUTHORIZATION_ACTIVTY_ACTION = "auth_activity_action";
    public static final String SHOP_ITEM_ARG = "SHOP_ITEM_ARG";

    public static String MAIN_FRAGMENT_TAG= "main_fragment_tag";
    public static String SEARCH_FRAGMENT_TAG= "search_fragment_tag";
    public static String SHOW_LOGIN_SCREEN = "show_login_screen";

    public static final String NAME = "NUGIFARM";
    public static final String USER_ID = "user_id";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PASSWORD = "user_password";
    public static final String USER_FULL_NAME = "full_name";
    public static final String USER_IS_FARMER = "is_farmer";

    public static final String USER_AGREEMENT = "user_agreement";
    public static final String REMEMBER_ME = "remember_me";
    public static final String ENABLING_PUSH_NOTIFICATIONS = "enabling_push_notifications";
    public static final String SHOW_ONLY_FRIENDS = "friends_only";
    public static final String DISCOVER_ENABLED = "friends_only";
    public static final String CATEG_INTENT_EXTRA = "CATEG_INTENT_EXTRA";

    public static final float MAP_ZOOM = 30.0f;

}
