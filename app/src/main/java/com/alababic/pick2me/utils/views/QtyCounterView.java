package com.alababic.pick2me.utils.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alababic.pick2me.R;

import rx.subjects.PublishSubject;

/**
 * Created by abdulmujibaliu on 11/12/17.
 */

public class QtyCounterView extends LinearLayout {
    private final View mView;
    private ImageView mPlusBtn;
    private TextView mMultiplierText;
    private ImageView mMinusBtn;
    private String TAG = getClass().getSimpleName();
    private int currentQty = 0;
    private QtyChangeListener mOnQtyChangedListener;

    public QtyCounterView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = inflater.inflate(R.layout.qty_counter_view, this, true);

        initView();


        mPlusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                currentQty = currentQty + 1;
                setmMultiplierText(currentQty);
            }
        });

        mMinusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                currentQty = currentQty - 1;
                setmMultiplierText(currentQty);
            }
        });

    }


    public void setOnQtyChangedListener(QtyChangeListener onQtyChangedListener) {
        mOnQtyChangedListener = onQtyChangedListener;
    }

    private void setmMultiplierText(int i) {
        Log.d(TAG, "Current Qty changed: "+ i);
        mMultiplierText.setText(String.valueOf(i));
        if (mOnQtyChangedListener != null) {
            mOnQtyChangedListener.onQtyChnaged(i);
        }
    }

    private void initView() {
        mPlusBtn = (ImageView) findViewById(R.id.plus_btn);
        mMultiplierText = (TextView) findViewById(R.id.multiplier_text);
        mMinusBtn = (ImageView) findViewById(R.id.minus_btn);
    }


    public interface QtyChangeListener {
        void onQtyChnaged(int newQty);
    }
}
