package com.alababic.pick2me.utils;

import android.content.Context;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alababic.pick2me.base.BaseApplication;

/**
 * Created by abdulmujibaliu on 7/18/17.
 */

public class DialogUtils {

    static MaterialDialog materialDialog;

    public static void loadingDialog(Context context, @StringRes int text) {
        makeDialog(context, BaseApplication.getInstance().getString(text), true);
    }

    public static void loadingDialog(Context context, String text) {
        makeDialog(context, text, true);
    }

    public static void alertDialog(Context context, @StringRes int text) {
        makeDialog(context, BaseApplication.getInstance().getString(text), false);
    }

    public static void alertDialog(Context context, String text) {
        makeDialog(context, text, false);
    }

    public static void alertDialog(Context context, String text, MaterialDialog.SingleButtonCallback singleButtonCallback) {
        makeDialog(context, text, singleButtonCallback);
    }

    private static void makeDialog(Context context, String text, MaterialDialog.SingleButtonCallback singleButtonCallback) {
        materialDialog = new MaterialDialog.Builder(context)
                .content(text).onPositive(singleButtonCallback).positiveText("OK")
                .build();
        materialDialog.show();
    }

    private static void makeDialog(Context context, String text, boolean isProgress) {
        materialDialog = new MaterialDialog.Builder(context)
                .content(text)
                .cancelable(!isProgress)
                .progress(isProgress, 0).build();
        materialDialog.show();
    }

    public static void hideDialog() {
        materialDialog.dismiss();
    }

}
