package com.alababic.pick2me.utils;

import android.net.Uri;

import io.reactivex.Observable;


/**
 * Created by ABDUL-MUJEEB ALIU on 27/11/2017.
 */

public interface ImagePickerListener {

    Observable<Uri> getImagePickerURIObservable();
}
