package com.alababic.pick2me.utils.views.thumbnailpickerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.productdetails.interfaces.OnThumbnailsClickedListener;
import com.alababic.pick2me.utils.PicassoCache;

import java.util.List;

/**
 * Created by abdulmujibaliu on 8/12/17.
 */

public class FilesRecyclerAdapter extends RecyclerView.Adapter<FilesRecyclerAdapter.AttachmentViewHolder> {

    protected List<String> attachmentList;
    protected OnThumbnailsClickedListener recyclerClickListener;
    protected Context mContext;
    protected String TAG = getClass().getSimpleName();

    public FilesRecyclerAdapter(List<String> attachmentList, Context context) {
        this.attachmentList = attachmentList;
        this.mContext = context;
    }

    @Override
    public AttachmentViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.thumbnail_view, parent, false);
        return new AttachmentViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final AttachmentViewHolder holder, final int position) {
        final String attachment = attachmentList.get(position);
        holder.bindAttachment(attachment);

    }

    @Override
    public int getItemCount() {
        return attachmentList.size();
    }

    public void addAll(List<String> imagesList) {
        attachmentList.addAll(imagesList);
        this.notifyDataSetChanged();
    }

    public class AttachmentViewHolder extends RecyclerView.ViewHolder {
        ImageView fileIcon;

        AttachmentViewHolder(View view) {
            super(view);

            fileIcon = view.findViewById(R.id.file_icon);
        }


        public void bindAttachment(String attachment) {
            PicassoCache.getPicassoInstance(fileIcon.getContext()).load(attachment).resize(50, 50).into(fileIcon);
            fileIcon.setOnClickListener(view -> recyclerClickListener.thumbNailClicked(attachment));
        }

    }

    public void setThumbNailClickListener(OnThumbnailsClickedListener recyclerClickListener) {
        this.recyclerClickListener = recyclerClickListener;
    }



}
