package com.alababic.pick2me.search;

import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.alababic.pick2me.R;
import com.claudiodegio.msv.MaterialSearchView;
import com.claudiodegio.msv.OnFilterViewListener;
import com.claudiodegio.msv.model.Filter;

import java.util.List;

public class SearchActivity extends AppCompatActivity implements OnFilterViewListener {

    private MaterialSearchView mSearchView;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        ActionBar actionBar;
        if ((actionBar = getSupportActionBar()) != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setElevation(0);
        }

        mSearchView = (MaterialSearchView) findViewById(R.id.sv);
/*
if I am using tabs ...

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
*/

        /*tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }*/

        //FilterMaterialSearchView cast = mSearchView;

        /*Section section = new Section("Categories");

        cast.addSection(section);

        Filter filter = new Filter(1, "Architecture", 0, R.drawable.architecture, getResources().getColor(R.color.green));
        cast.addFilter(filter);

        filter = new Filter(1, "Arts and Humanites", 0, R.drawable.art_literature, getResources().getColor(R.color.blue_green));
        cast.addFilter(filter);

        filter = new Filter(1, "Law", 0, R.drawable.law, getResources().getColor(R.color.blue_krayola));
        cast.addFilter(filter);*/

  /*      filter = new Filter(1, "Business", 0, R.drawable.bussiness, getResources().getColor(R.color.mauveine));
        cast.addFilter(filter);

        filter = new Filter(1, "Life Science", 0, R.drawable.life_sci, getResources().getColor(R.color.may_green));
        cast.addFilter(filter);

        filter = new Filter(1, "Medicine", 0, R.drawable.medicine_health, getResources().getColor(R.color.btnRequest));
        cast.addFilter(filter);

        filter = new Filter(1, "Physical Sciences and Maths", 0, R.drawable.phy_math_sci, getResources().getColor(R.color.contact_subhead_text_color));
        cast.addFilter(filter);
*/

        //cast.setOnFilterViewListener(this);



    }

/*
//When using tabulated search
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(SearchResultFragment.newInstance(), "JOURNALS");
        adapter.addFrag(SearchResultFragment.newInstance(), "PAPERS");
        adapter.addFrag(SearchResultFragment.newInstance(), "REPORTS");
        adapter.addFrag(SearchResultFragment.newInstance(), "MORE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon

            return mFragmentTitleList.get(position);
        }
    }
*/


    @Override
    protected void onPostResume() {
        super.onPostResume();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSearchView.showSearch(true);
            }
        }, 50);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        mSearchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFilterAdded(Filter filter) {

    }

    @Override
    public void onFilterRemoved(Filter filter) {

    }

    @Override
    public void onFilterChanged(List<Filter> list) {

    }
}
