package com.alababic.pick2me.search.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.alababic.pick2me.R;

/**
 * Created by Abdul-Mujeeb Aliu on 2/7/2017.
 */

public class SearchResultFragment extends Fragment {

    private Context mContext;
    private View view;
    private ProgressBar mProgressBar;

    public static SearchResultFragment newInstance() {
        return new SearchResultFragment();
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mContext = getContext();
        view = inflater.inflate(R.layout.fragment_result_search, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        hasOptionsMenu();
        //mProgressBar.setVisibility(View.GONE);
        return view;
    }

}
