package com.alababic.pick2me.import2me.fragments;

/**
 * Created by abdulmujibaliu on 3/9/17.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.R;


public class Import2MeAdderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public Import2MeAdderFragment() {
    }

    public static Import2MeAdderFragment newInstance() {
        Import2MeAdderFragment fragment = new Import2MeAdderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_import_2_me_adder, container, false);


        return rootView;
    }



}