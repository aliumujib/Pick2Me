/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/5/17 4:30 PM
 */

package com.alababic.pick2me.import2me;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.alababic.pick2me.R;
import com.alababic.pick2me.base.BaseActivity;
import com.alababic.pick2me.utils.ImagePickerListener;
import com.alababic.pick2me.utils.commons.Constants;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public class Import2MeActivity extends BaseActivity implements ImagePickerListener {

    private ActionBar mActionBar;
    private PublishSubject<Uri> mImagePickerURISubject  = PublishSubject.create();

    public static void start(Context context) {
        Intent intent = new Intent(context, Import2MeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_to_me);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);

        if ((mActionBar = getSupportActionBar()) != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == Constants.REQUEST_CAMERA_MAIN) {
            Uri photoUri = data.getData();
            mImagePickerURISubject.onNext(photoUri);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public Observable<Uri> getImagePickerURIObservable() {
        return mImagePickerURISubject;
    }
}
