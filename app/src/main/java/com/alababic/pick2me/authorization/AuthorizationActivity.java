package com.alababic.pick2me.authorization;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.alababic.pick2me.R;
import com.alababic.pick2me.authorization.fragments.login.AuthorizationLoginFragment;
import com.alababic.pick2me.authorization.fragments.LauncherFragment;
import com.alababic.pick2me.base.BaseActivity;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.utils.commons.Constants;


public class AuthorizationActivity extends BaseActivity {

    private LauncherFragment mLauncherFragment;
    private AuthorizationLoginFragment mAuthorizationLoginFragment;

    public static void start(Context context, String action) {
        Intent intent = new Intent(context, AuthorizationActivity.class);
        intent.putExtra(Constants.AUTHORIZATION_ACTIVTY_ACTION, action);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        mLauncherFragment = new LauncherFragment();
        mAuthorizationLoginFragment = new AuthorizationLoginFragment();

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(Constants.AUTHORIZATION_ACTIVTY_ACTION)) {
                Utils.replaceFragmentWithorWithoutBackState(mAuthorizationLoginFragment, this, Constants.WITHOUTBSTATE);
            } else {
                Utils.replaceFragmentWithorWithoutBackState(mLauncherFragment, this, Constants.WITHOUTBSTATE);
            }
        } else {
            Utils.replaceFragmentWithorWithoutBackState(mLauncherFragment, this, Constants.WITHOUTBSTATE); ///For some reason that I am to sleepy to figure out,
/// the app opens and shows nothung without thius line, just leave it be, JK .... Do that if the intent is null, could methodize but meh!!
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }
}
