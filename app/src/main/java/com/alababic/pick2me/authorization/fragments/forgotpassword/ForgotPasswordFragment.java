package com.alababic.pick2me.authorization.fragments.forgotpassword;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.DialogUtils;
import com.alababic.pick2me.utils.ToastUtils;
import com.alababic_app.pick2me_data.models.PasswordResetData;
import com.alababic_app.pick2me_data.repositories.AuthRepository;
import com.neopixl.pixlui.components.edittext.EditText;

import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;

/**
 * Created by abdulmujibaliu on 11/12/17.
 */

public class ForgotPasswordFragment extends Fragment {

    private LinearLayout mInputAndControls;
    private EditText mInputUserEmail;
    private AppCompatButton mBtnNextComplete;

    public ForgotPasswordFragment() {
    }

    public static ForgotPasswordFragment newInstance() {

        Bundle args = new Bundle();

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);


    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_details_auth, container, false);
    }

    private void initView(View convertView) {
        this.mInputAndControls = (LinearLayout) convertView.findViewById(R.id.input_and_controls);
        this.mInputUserEmail = (EditText) convertView.findViewById(R.id.input_user_email);
        this.mBtnNextComplete = (AppCompatButton) convertView.findViewById(R.id.btn_next_complete);

        Toolbar toolbar = (Toolbar) convertView.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar;
        if ((actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar()) != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Register");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp);
        }

        mBtnNextComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtils.loadingDialog(getContext(), "Resetting password");
                resetPassword(new PasswordResetData(mInputUserEmail.getText().toString(), "Y"));
            }
        });

    }

    private void handleSuccess(String response) {
        DialogUtils.hideDialog();
        ToastUtils.shortToast(response);
    }

    private void handleFail(Throwable throwable) {
        DialogUtils.hideDialog();
        throwable.printStackTrace();
        ToastUtils.shortToast(throwable.getMessage());
    }

    private void resetPassword(PasswordResetData data) {
        AuthRepository.sharedInstance.resetUserPassword(getContext(), data).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                handleSuccess("Password reset successfully");
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                handleFail(throwable);
            }
        });
    }
}
