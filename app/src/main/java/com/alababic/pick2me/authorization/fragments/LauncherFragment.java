package com.alababic.pick2me.authorization.fragments;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.R;
import com.alababic.pick2me.authorization.fragments.login.AuthorizationLoginFragment;
import com.alababic.pick2me.mainpage.MainActivity;
import com.alababic.pick2me.utils.commons.Constants;

/**
 * A placeholder fragment containing a simple view.
 */
public class LauncherFragment extends Fragment {

    private AuthorizationLoginFragment mAuthorizationLoginFragment;

    private static Fragment newInstance() {
        Fragment launcherFragment = new LauncherFragment();
        Bundle args = new Bundle();
        args.putString(Constants.AUTHORIZATION_ACTIVTY_ACTION, Constants.SHOW_LOGIN_SCREEN);
        launcherFragment.setArguments(args);
        return launcherFragment;
    }

    public LauncherFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launcher, container, false);


        mAuthorizationLoginFragment = new AuthorizationLoginFragment();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                    MainActivity.start(getContext());
            }
        }, 2000);

        return view;
    }


}
