package com.alababic.pick2me.authorization.fragments.register;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.DialogUtils;
import com.alababic.pick2me.utils.ToastUtils;
import com.alababic_app.pick2me_data.models.User;
import com.alababic_app.pick2me_data.repositories.AuthRepository;
import com.neopixl.pixlui.components.edittext.EditText;
import com.neopixl.pixlui.components.textview.TextView;

import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;

/**
 * Created by abdulmujibaliu on 10/2/17.
 */
@SuppressLint("ValidFragment")
public class RegisterFragment extends Fragment {

    private LinearLayout mInputAndControls;
    private EditText mInputUserName;
    private EditText mInputEmail;
    private EditText mInputUserPassword;
    private EditText mInputConfirmPassword;
    private LinearLayout mSociaNetworkLinks;
    private AppBarLayout mActionBar;
    private Toolbar mToolbar;
    private AppCompatButton mBtnNextComplete;
    private LinearLayout mTandcLinks;
    private TextView mTextView7;
    private TextView mSignIn;

    public RegisterFragment() {
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);

    }

    private void initView(View view) {
        mInputAndControls = (LinearLayout) view.findViewById(R.id.input_and_controls);
        mInputUserName = (EditText) view.findViewById(R.id.input_user_name);
        mInputEmail = (EditText) view.findViewById(R.id.input_email);
        mInputUserPassword = (EditText) view.findViewById(R.id.input_user_password);
        mInputConfirmPassword = (EditText) view.findViewById(R.id.input_confirm_password);
        mSociaNetworkLinks = (LinearLayout) view.findViewById(R.id.socia_network_links);
        mBtnNextComplete = (AppCompatButton) view.findViewById(R.id.btn_next_complete);
        mSignIn = (TextView) view.findViewById(R.id.sign_in);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar;
        if ((actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar()) != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Register");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp);
        }


        mBtnNextComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User(mInputUserName.getText().toString(), mInputUserPassword.getText().toString(),
                        mInputEmail.getText().toString(), mInputConfirmPassword.getText().toString());
                DialogUtils.loadingDialog(getContext(), "Signing Up");
                signUserUp(user);
            }
        });

    }

    private void signUserUp(User user) {
        AuthRepository.sharedInstance.signUserUp(getContext(), user).subscribe(new Consumer<User>() {
            @Override
            public void accept(User user) throws Exception {
                handleSuccess(user.getUsername());
            }
        }, this::handleFail);
    }

    private void handleSuccess(String response) {
        DialogUtils.hideDialog();
        ToastUtils.shortToast(response);
    }

    private void handleFail(Throwable throwable) {
        DialogUtils.hideDialog();
        throwable.printStackTrace();
        ToastUtils.shortToast(throwable.getMessage());
    }
}
