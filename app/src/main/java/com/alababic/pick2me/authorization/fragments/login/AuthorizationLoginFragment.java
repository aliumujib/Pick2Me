package com.alababic.pick2me.authorization.fragments.login;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.authorization.fragments.register.RegisterFragment;
import com.alababic.pick2me.authorization.fragments.forgotpassword.ForgotPasswordFragment;

import com.alababic.pick2me.base.BaseFragment;
import com.alababic.pick2me.utils.DialogUtils;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.utils.commons.Constants;
import com.alababic_app.pick2me_data.models.User;
import com.alababic_app.pick2me_data.repositories.AuthRepository;
import com.alababic_app.pick2me_data.repositories.UserRepository;

import io.reactivex.Notification;
import io.reactivex.functions.Consumer;

/**
 * Created by Abdul-Mujeeb Aliu on 7/22/2016.
 */
public class AuthorizationLoginFragment extends BaseFragment {

    private RegisterFragment mAuthorizationRegisterFragment;
    private TextView mSignupTextView, mForgotPassword;
    private EditText mUserNmaeET;
    private EditText mPasswordEt;
    private AppCompatButton mButton;
    private Context mContext;

    private String TAG = getClass().getSimpleName();

    public AuthorizationLoginFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_authorization_login, container, false);

        mContext = getContext();
        mAuthorizationRegisterFragment = new RegisterFragment();
        mSignupTextView = (TextView) view.findViewById(R.id.sign_up);
        mForgotPassword = (TextView) view.findViewById(R.id.forgot_password);
        mUserNmaeET = view.findViewById(R.id.input_user_name);
        mPasswordEt = (EditText) view.findViewById(R.id.input_password);
        mButton = (AppCompatButton) view.findViewById(R.id.btn_signup);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //handleLogin();
                User loginData = new User(mUserNmaeET.getText().toString(), mPasswordEt.getText().toString());
                loginUSer(loginData);
            }
        });


        mSignupTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.replaceFragmentWithorWithoutBackState(mAuthorizationRegisterFragment, getContext(), Constants.WITHOUTBSTATE);
            }
        });

        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.replaceFragmentWithorWithoutBackState(ForgotPasswordFragment.newInstance(), getContext(), Constants.WITHOUTBSTATE);
            }
        });

        return view;
    }

    private void loginUSer(User user) {
        DialogUtils.loadingDialog(getContext(), "Please wait");
        AuthRepository.sharedInstance.signUserIn(getContext(), user).subscribe(user1 ->{
            //handleSuccess(TAG);
            handleLoginSuccess(user1);
            Log.d(TAG, user1.getUsername());
        }, this::handleFail);
    }

    void handleLoginSuccess(User user){
        UserRepository.sharedInstance.saveCurrentUser(user).subscribe(new Consumer<Notification<Boolean>>() {
            @Override
            public void accept(Notification<Boolean> booleanNotification) throws Exception {
                if(booleanNotification.isOnError()){
                    handleFail(booleanNotification.getError());
                }else {
                    handleSuccess("Sign in successfull");
                    getActivity().finish();
                }
            }
        });
    }

}
