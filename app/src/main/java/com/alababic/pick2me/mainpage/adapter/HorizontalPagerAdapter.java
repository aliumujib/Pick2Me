/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/7/17 3:19 AM
 */

package com.alababic.pick2me.mainpage.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.shop_details.ShopDetailsActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


/**
 * Created by GIGAMOLE on 7/27/16.
 */
public class HorizontalPagerAdapter extends PagerAdapter {

    private final LibraryObject[] LIBRARIES = new LibraryObject[]{
            new LibraryObject(
                    "file:///android_asset/photos/legumes.jpeg",
                    "Legumes"
            ),
            new LibraryObject(
                    "file:///android_asset/photos/grains.jpg",
                    "Grains"
            ),
            new LibraryObject(
                    "file:///android_asset/photos/rootcrop.jpeg",
                    "Root Crops"
            ),
            new LibraryObject(
                    "file:///android_asset/photos/seafood.jpg",
                    "Sea Foods"
            ),
            new LibraryObject(
                    "file:///android_asset/photos/spices.jpeg",
                    "Spices"
            ),
            new LibraryObject(
                    "file:///android_asset/photos/fibrous.jpeg",
                    "Fibre Crops"
            ),
            new LibraryObject(
                    "file:///android_asset/photos/oilandseed.jpeg",
                    "Oil and Seeds"
            )
    };

    protected Context mContext;
    protected LayoutInflater mLayoutInflater;

    private boolean mIsTwoWay;

    public HorizontalPagerAdapter(final Context context, final boolean isTwoWay) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mIsTwoWay = isTwoWay;
    }

    @Override
    public int getCount() {
        return mIsTwoWay ? 6 : LIBRARIES.length;
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view;

        view = mLayoutInflater.inflate(R.layout.slider_item, container, false);
        setupItem(view, LIBRARIES[position], mContext);

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }

    public static void setupItem(final View view, final LibraryObject libraryObject, final Context mContext) {
        final TextView txt = (TextView) view.findViewById(R.id.item_desc);
        txt.setText(libraryObject.getTitle());

        final ImageView img = (ImageView) view.findViewById(R.id.img_item);
        //img.setImageResource(libraryObject.getRes());
        Picasso.with(mContext)
                .load(libraryObject.getRes())
                .resize(400, 200).centerCrop()
                .placeholder(R.drawable.shovel_ground)
                .error(R.drawable.shovel_ground)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        assert img != null;
                        img.setImageBitmap(bitmap);
                        Palette.from(bitmap)
                                .generate(new Palette.PaletteAsyncListener() {
                                    @Override
                                    public void onGenerated(Palette palette) {
                                        Palette.Swatch textSwatch = palette.getVibrantSwatch();
                                        if (textSwatch == null) {
                                            //Toast.makeText(mContext, "Null swatch :(", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        //backgroundGroup.setBackgroundColor(textSwatch.getRgb());
                                        txt.setTextColor(textSwatch.getRgb());
                                        //bodyColorText.setTextColor(textSwatch.getBodyTextColor());
                                    }
                                });
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ShopDetailsActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    public static class LibraryObject {

        private String mTitle;
        private String mRes;

        public LibraryObject(final String url, final String title) {
            mRes = url;
            mTitle = title;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(final String title) {
            mTitle = title;
        }

        public String getRes() {
            return mRes;
        }

        public void setRes(final String res) {
            mRes = res;
        }
    }

}
