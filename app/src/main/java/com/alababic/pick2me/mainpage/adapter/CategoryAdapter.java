package com.alababic.pick2me.mainpage.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.mainpage.fragments.OnCategoryClickListener;
import com.alababic.pick2me.utils.PicassoCache;
import com.alababic_app.pick2me_data.models.Category;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by aliuabdulmujib on 24/04/15.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    List<Object> contents;
    private static final String TAG = "SERMONSRVSADAPTER";
    private Context mContext;
    private OnCategoryClickListener categoryClickListener;
    //Arraylists to get
    private static List<Category> mCategoryList = new ArrayList<>();
    private static List<Category> mTempList = new ArrayList<>();

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;

    public CategoryAdapter(Context context, List<Category> sermonsList, OnCategoryClickListener categoryClickListener) {
        this.mCategoryList = sermonsList;
        this.mContext = context;
        //this.mTempList = new ArrayList<>(sermonsList.subList(0, 8));
        this.categoryClickListener = categoryClickListener;
    }

    public void refresh() {
        notifyItemRangeChanged(0, mCategoryList.size());
    }


    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return TYPE_CELL;
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_material_for_scrolling_list, parent, false);
        return new CategoryAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Category currentCategory = mCategoryList.get(position);
        holder.bindItem(currentCategory);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mRecourceImage;
        public TextView mRecourseTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mRecourceImage = (ImageView) itemView.findViewById(R.id.item_icon);
            mRecourseTextView = (TextView) itemView.findViewById(R.id.item_name);
        }

        public void bindItem(final Category currentCategory) {
            if (currentCategory.getRecoursePicID() != 0) {
                PicassoCache.getPicassoInstance(mContext).load(currentCategory.getRecoursePicID())
                        .resize(280, 280).centerInside()
                        .into(this.mRecourceImage);

                this.mRecourceImage.setBackgroundResource(R.drawable.rect_blue);
                if(this.mRecourceImage.getBackground()!=null){
                    Drawable backgroundDrawable = DrawableCompat.wrap(this.mRecourceImage.getBackground()).mutate();
                    DrawableCompat.setTint(backgroundDrawable, ContextCompat.getColor(mContext, currentCategory.getRecourceBgColorID()));
                }

                this.mRecourseTextView.setText(currentCategory.getCategName());
                this.mRecourceImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                    Intent intent = new Intent(mContext, ShopDetailsActivity.class);
//                    mContext.startActivity(intent);
                        itemView.setOnClickListener(this);
                        categoryClickListener.onCategoryClicked(currentCategory);
                    }
                });
            }

        }
    }
}
