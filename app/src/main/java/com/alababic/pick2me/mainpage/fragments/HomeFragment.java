package com.alababic.pick2me.mainpage.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.R;
import com.alababic.pick2me.buy2me.Buy2MeActivity;
import com.alababic.pick2me.import2me.Import2MeActivity;
import com.alababic.pick2me.shops.ShopsActivity;
import com.alababic_app.pick2me_data.repositories.CategoriesRepository;
import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.alababic.pick2me.mainpage.adapter.HorizontalPagerAdapter;
import com.alababic.pick2me.mainpage.adapter.CategoryAdapter;
import com.alababic_app.pick2me_data.models.Category;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by Abdul-Mujeeb Aliu on 7/23/2016.
 */
public class HomeFragment extends Fragment implements OnCategoryClickListener {

    private RecyclerView mRecourceImagesRecyclerView;
    private GridLayoutManager mLinearLayoutManager;
    private CategoryAdapter mAdapter;
    private Context mContext;
    private List<Category> mCategories = new ArrayList<>();

    public HomeFragment() {

    }


    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mRecourceImagesRecyclerView = (RecyclerView) view.findViewById(R.id.recources_recyclerView);

        int spanCount = 2; // 2 columns
        int spacing = 8; // 4px

        mLinearLayoutManager = new GridLayoutManager(getContext(), spanCount);
        mRecourceImagesRecyclerView.setLayoutManager(mLinearLayoutManager);
        //mRecourceImagesRecyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, true));
        mAdapter = new CategoryAdapter(getContext(), mCategories, this);
        mRecourceImagesRecyclerView.setAdapter(mAdapter);

        CategoriesRepository.sharedInstance.getCategoryData().subscribe(new Consumer<List<Category>>() {
            @Override
            public void accept(List<Category> categories) throws Exception {
                mCategories.clear();
                mCategories.addAll(categories);
                mAdapter.notifyDataSetChanged();
            }
        });

        mContext = getContext();

        final HorizontalInfiniteCycleViewPager horizontalInfiniteCycleViewPager =
                (HorizontalInfiniteCycleViewPager) view.findViewById(R.id.hicvp);
        horizontalInfiniteCycleViewPager.setAdapter(new HorizontalPagerAdapter(getContext(), false));

        return view;
    }


    @Override
    public void onCategoryClicked(@NotNull Category category) {
        if(category.getCategName().contains("Buy")){
            Buy2MeActivity.start(getContext());
        }else if(category.getCategName().contains("Import")){
            Import2MeActivity.start(getContext());
        }
        else {
            ShopsActivity.start(getContext(), category);
        }


    }
}
