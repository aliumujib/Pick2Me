package com.alababic.pick2me.mainpage.fragments

import com.alababic_app.pick2me_data.models.Category

/**
 * Created by abdulmujibaliu on 10/22/17.
 */
interface OnCategoryClickListener {
    fun onCategoryClicked(category: Category)
}