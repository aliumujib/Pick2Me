package com.alababic.pick2me.mainpage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.addnewproduct.AddNewProductActivity;
import com.alababic.pick2me.authorization.AuthorizationActivity;
import com.alababic.pick2me.base.BaseActivity;
import com.alababic.pick2me.cart.CartActivity;
import com.alababic.pick2me.farmersfarm.FamersFarmActivity;
import com.alababic.pick2me.mainpage.fragments.HomeFragment;
import com.alababic.pick2me.buy2me.Buy2MeActivity;
import com.alababic.pick2me.profile.fragments.UserProfileFragment;
import com.alababic.pick2me.shops.ShopsActivityFragment;
import com.alababic.pick2me.utils.commons.Constants;
import com.alababic.pick2me.search.SearchActivity;
import com.alababic.pick2me.profile.UserProfileActivity;
import com.alababic_app.pick2me_data.repositories.UserRepository;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.functions.Consumer;
import xyz.belvi.luhn.Luhn;
import xyz.belvi.luhn.cardValidator.models.LuhnCard;
import xyz.belvi.luhn.interfaces.LuhnCallback;
import xyz.belvi.luhn.interfaces.LuhnCardVerifier;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private LinearLayout mHeaderlayout;
    private Context mContext;
    private String[] mCategoriesList = {"Drugs", "Food", "Flowers", "Clothes"};
    protected BottomNavigationView mBottomNavigation;
    private Toolbar toolbar;

    private void animateLopperTextForever(final TextView textView) {
        final Animation animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setDuration(1000);
        animation1.setStartOffset(2000);

        final Animation animation2 = new AlphaAnimation(1.0f, 0.0f);
        animation2.setDuration(1000);
        animation2.setStartOffset(2000);

        //animation1 AnimationListener
        animation1.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation2 when animation1 ends (continue)
                textView.startAnimation(animation2);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub
                textView.setText(mCategoriesList[new Random().nextInt((3) + 1)]);
            }

        });


        //animation2 AnimationListener
        animation2.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation arg0) {
                // start animation1 when animation2 ends (repeat)
                textView.startAnimation(animation1);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub
                //textView.setText(mCategoriesList[new Random().nextInt((3))]);
            }

        });

        textView.startAnimation(animation1);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }


    protected void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        initNavDrawer(toolbar);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar;


        TextView randromTextView = (TextView) findViewById(R.id.journalss_tv);

        if ((actionBar = getSupportActionBar()) != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            animateLopperTextForever(randromTextView);
            actionBar.setTitle("Pick2me");
        }

        mContext = this;
        initFragmentArrayList();

        replaceCurrentFragmentWithoutBackState(mFragments.get(0));


        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.search_button);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SearchActivity.class);
                startActivity(intent);
            }
        });
    }


    private void initNavDrawer(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mBottomNavigation = (BottomNavigationView) findViewById(R.id.navigation);

        mBottomNavigation.setOnNavigationItemSelectedListener(mOnBottomNavigationItemSelectedListener);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mHeaderlayout = (LinearLayout) navigationView.getHeaderView(0);
        CircleImageView circularImageView = (CircleImageView) mHeaderlayout.findViewById(R.id.imageView);

        UserRepository.sharedInstance.isUserSignedIn().subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                if (aBoolean) {
                    inflateBottomNavigation(R.menu.navigation_logged_in);
                    navigationView.getMenu().clear(); //clear old inflated items.
                    navigationView.inflateMenu(R.menu.activity_main_drawer);
                    circularImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, UserProfileActivity.class);
                            startActivity(intent);
                        }
                    });
                } else {
                    navigationView.getMenu().clear(); //clear old inflated items.
                    navigationView.inflateMenu(R.menu.activity_main_not_logged_in);
                    inflateBottomNavigation(R.menu.navigation_not_logged_in);
                    circularImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AuthorizationActivity.start(mContext, Constants.AUTHORIZATION_ACTIVTY_ACTION);
                        }
                    });
                }
            }
        });


    }

    private void inflateBottomNavigation(@MenuRes int menuResID) {
        mBottomNavigation.getMenu().clear();
        mBottomNavigation.inflateMenu(menuResID);
        disableShiftMode(mBottomNavigation);
    }


    protected ArrayList<Fragment> mFragments = new ArrayList<>();

    private void initFragmentArrayList() {
        mFragments.add(HomeFragment.newInstance());
        mFragments.add(ShopsActivityFragment.newInstance());
        mFragments.add(UserProfileFragment.newInstance());
       /* mFragments.add(SpacerProfileFragment.newInstance());
        mFragments.add(BaseNotificationsFragment.newInstance());
        mFragments.add(RequestTabsFragment.newInstance());*/

    }

    protected BottomNavigationView.OnNavigationItemSelectedListener mOnBottomNavigationItemSelectedListener
            = item -> {

        switch (item.getItemId()) {
            case R.id.navigation_home:
                replaceCurrentFragmentWithBackState(mFragments.get(0));
                return true;
            case R.id.navigation_favorites:
                //                    if (isUserLoggedIn()) {
                //                        replaceCurrentFragmentWithBackState(mFragments.get(1));
                //                    } else {
                //                        replaceCurrentFragmentWithBackState(SpacerSignInToUseFragment.newInstance());
                //                    }
                return true;
            case R.id.navigation_shops:
                replaceCurrentFragmentWithBackState(mFragments.get(1));
                //                    if (isUserLoggedIn()) {
                //                        replaceCurrentFragmentWithBackState(mFragments.get(2));
                //                    } else {
                //                        replaceCurrentFragmentWithBackState(SpacerSignInToUseFragment.newInstance());
                //                    }
                return true;
            case R.id.navigation_orders:
                //                    if (isUserLoggedIn()) {
                //                        replaceCurrentFragmentWithBackState(mFragments.get(3));
                //                    } else {
                //                        replaceCurrentFragmentWithBackState(SpacerSignInToUseFragment.newInstance());
                //                    }
                return true;
            case R.id.navigation_settings:
                replaceCurrentFragmentWithBackState(mFragments.get(2));
                //                    if (isUserLoggedIn()) {
                //                        replaceCurrentFragmentWithBackState(mFragments.get(1));
                //                    } else {
                //                        //replaceCurrentFragmentWithBackState(SpacerSignInToUseFragment.newInstance());
                //                    }
                return true;

        }
        return false;
    };


    private boolean isUserFarmer() {
        if (!mSharedHelperInstance.getIsFarmer()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                CartActivity.start(mContext);
                return true;
            default:
                return false;
        }
    }

    public void openCardData(){
        Luhn.startLuhn(this, new LuhnCallback() {
            @Override
            public void cardDetailsRetrieved(Context luhnContext, LuhnCard creditCard, final LuhnCardVerifier cardVerifier) {
                cardVerifier.startProgress();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cardVerifier.requestOTP(4);
                    }
                }, 2500);
            }

            @Override
            public void otpRetrieved(Context luhnContext, final LuhnCardVerifier cardVerifier, String otp) {
                cardVerifier.startProgress();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cardVerifier.onCardVerified(false, getString(R.string.verification_error), getString(R.string.verification_error));
                    }
                }, 2500);
            }

            @Override
            public void onFinished(boolean isVerified) {
                // luhn has finished. Do something
            }
        }, R.style.LuhnStyle);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            openCardData();
        } else if (id == R.id.nav_cart) {
            CartActivity.start(mContext);
        } else if (id == R.id.nav_transactions) {

        } else if (id == R.id.nav_messages) {
            Buy2MeActivity.start(mContext);
        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_famer_farm) {
            FamersFarmActivity.start(mContext);
        } else if (id == R.id.add_new_product) {
            AddNewProductActivity.start(mContext);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
}
