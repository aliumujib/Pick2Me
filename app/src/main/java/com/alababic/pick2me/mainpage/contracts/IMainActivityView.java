package com.alababic.pick2me.mainpage.contracts;

/**
 * Created by abdulmujibaliu on 10/7/17.
 */

public interface IMainActivityView {

    void hideSearchBar();

    void showSearchBar();

}
