/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/10/17 12:15 PM
 */

package com.alababic.pick2me.addnewproduct.fragments;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.badoualy.stepperindicator.StepperIndicator;
import com.alababic.pick2me.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;


/**
 * A placeholder fragment containing a simple view.
 */
public class AddNewProductFragment extends Fragment {

    public AddNewProductFragment() {
    }

    private ViewPager mViewPager;
    private Button mNextCompleteButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new_product, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar;
        if ((actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar()) != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setTitle("Checkout");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp);
        }


        mNextCompleteButton = (Button) view.findViewById(R.id.btn_next_complete);


        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
        setupViewPager(mViewPager);

        final StepperIndicator indicator = (StepperIndicator) view.findViewById(R.id.stepper_indicator);
        indicator.setViewPager(mViewPager, false);

        indicator.addOnStepClickListener(new StepperIndicator.OnStepClickListener() {
            @Override
            public void onStepClicked(int step) {
                indicator.setCurrentStep(step);
                mViewPager.setCurrentItem(step, true);

            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    mNextCompleteButton.setText("Finish");
                    mNextCompleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getActivity().finish();
                        }
                    });
                } else if (position == 1) {
                    mNextCompleteButton.setText("Next");
                } else {
                    mNextCompleteButton.setText("Next");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mNextCompleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem() != 3) {
                    mViewPager.setCurrentItem(indicator.getCurrentStep() + 1);
                }
            }
        });

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new ProductDetails(), "ONE");
        adapter.addFrag(new ProductDescInfoFragment(), "TWO");
        adapter.addFrag(new CompleteConfirmFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }


    @SuppressLint("ValidFragment")
    public static class ProductDetails extends Fragment {

        private TextView mPrefDODEditText;

        private Calendar mDODelivery = Calendar.getInstance();


        DatePickerDialog.OnDateSetListener mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                mDODelivery.set(YEAR, year);
                mDODelivery.set(MONTH, monthOfYear);
                mDODelivery.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String date = sdf.format(mDODelivery.getTime().getTime());
                mPrefDODEditText.setText(date);
                System.out.println(date);
            }

        };

        public ProductDetails() {
        }

        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.product_info_fragment, container, false);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

           // mPrefDODEditText = (EditText) view.findViewById(R.id.prefferred_delivery_date);

           /* mPrefDODEditText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar now = Calendar.getInstance();

                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            mOnDateSetListener,
                            now.get(YEAR),
                            now.get(MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );

                    final android.app.FragmentManager manager = ((Activity) getActivity()).getFragmentManager();

                    dpd.show(manager, "Datepickerdialog");
                }
            });*/

        }
    }

    @SuppressLint("ValidFragment")
    public static class ProductDescInfoFragment extends Fragment {

        public ProductDescInfoFragment() {

        }


        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

        }

        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.product_desc_info_fragment, container, false);
        }
    }


    @SuppressLint("ValidFragment")
    public static class CompleteConfirmFragment extends Fragment {

        public CompleteConfirmFragment() {
        }

        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.product_complete_fragment, container, false);

            return view;
        }

    }
}
