/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/10/17 12:15 PM
 */

package com.alababic.pick2me.addnewproduct;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.alababic.pick2me.R;
import com.alababic.pick2me.base.BaseActivity;


public class AddNewProductActivity extends BaseActivity {


    public static void start(Context context){
        Intent intent = new Intent(context, AddNewProductActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_product);

    }



}
