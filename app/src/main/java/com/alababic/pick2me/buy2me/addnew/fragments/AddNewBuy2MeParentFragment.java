/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/7/17 12:29 AM
 */

package com.alababic.pick2me.buy2me.addnew.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alababic.pick2me.R;
import com.alababic.pick2me.buy2me.addnew.adapter.ViewPagerAdapter;
import com.badoualy.stepperindicator.StepperIndicator;


/**
 * A placeholder fragment containing a simple view.
 */
public class AddNewBuy2MeParentFragment extends Fragment {

    public AddNewBuy2MeParentFragment() {
    }


    private ViewPager mViewPager;
    private Button mNextCompleteButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_out_flow, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar;
        if ((actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar()) != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setTitle("Pick 2 Me");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp);
        }


        mNextCompleteButton = (Button) view.findViewById(R.id.btn_next_complete);


        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
        setupViewPager(mViewPager);

        final StepperIndicator indicator = (StepperIndicator) view.findViewById(R.id.stepper_indicator);
        indicator.setViewPager(mViewPager, false);

        indicator.addOnStepClickListener(new StepperIndicator.OnStepClickListener() {
            @Override
            public void onStepClicked(int step) {
                indicator.setCurrentStep(step);
                mViewPager.setCurrentItem(step, true);

            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    mNextCompleteButton.setText("Finish");
                } else if (position == 0) {
                    mNextCompleteButton.setText("Next");
                } else {
                    mNextCompleteButton.setText("Next");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mNextCompleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem() != 2) {
                    mViewPager.setCurrentItem(indicator.getCurrentStep() + 1);
                }
            }
        });

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new BasicDetailsB2MeFragment(), "ONE");
        adapter.addFrag(new Buy2MeFinalDetailsFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }




}
