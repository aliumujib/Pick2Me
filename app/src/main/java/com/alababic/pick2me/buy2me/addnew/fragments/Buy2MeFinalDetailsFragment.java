package com.alababic.pick2me.buy2me.addnew.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.alababic.pick2me.R;

/**
 * Created by ABDUL-MUJEEB ALIU on 26/11/2017.
 */

@SuppressLint("ValidFragment")
public  class Buy2MeFinalDetailsFragment extends Fragment {
    private EditText mItemDesc;
    private EditText mAddressFrom;
    private EditText mDeliverTo;
    private EditText mItemWebAddress;

    public Buy2MeFinalDetailsFragment() {
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_b2m_final, container, false);
    }

    @Override
    public void onViewCreated(View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        mItemDesc = rootView.findViewById(R.id.item_desc);
        mAddressFrom = rootView.findViewById(R.id.address_from);
        mDeliverTo = rootView.findViewById(R.id.deliver_to);
        mItemWebAddress = rootView.findViewById(R.id.item_web_address);

    }
}