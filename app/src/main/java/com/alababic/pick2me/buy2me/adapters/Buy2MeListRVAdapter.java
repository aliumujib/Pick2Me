package com.alababic.pick2me.buy2me.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.Utils;
import com.alababic_app.pick2me_data.models.Buy2MeItem;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ABDUL-MUJEEB ALIU on 25/11/2017.
 */

public class Buy2MeListRVAdapter extends RecyclerView.Adapter<Buy2MeListRVAdapter.RecyclerViewHolder> {
    private String TAG = getClass().getSimpleName();
    Context mContext;
    ArrayList<Buy2MeItem> mProfileArrayList;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private CardView mCardView;
        private ImageView mItemPhoto;
        private CircularImageView mSellerImage;
        private TextView mItemName2;
        private TextView mItemSeller;
        private TextView mItemExtras;

        public RecyclerViewHolder(View convertView) {
            super(convertView);
            this.mCardView = (CardView) convertView.findViewById(R.id.card_view);
            this.mItemPhoto = (ImageView) convertView.findViewById(R.id.item_photo);
            this.mSellerImage = (CircularImageView) convertView.findViewById(R.id.seller_image);
            this.mItemName2 = (TextView) convertView.findViewById(R.id.item_name_title2);
            this.mItemSeller = (TextView) convertView.findViewById(R.id.item_seller);
            this.mItemExtras = (TextView) convertView.findViewById(R.id.item_extras);
        }

        public void bindItem(Buy2MeItem buy2MeItem){
            Utils.loadImage(mItemPhoto, buy2MeItem.getPhotoImage(), mContext);
            Utils.loadImage(mSellerImage, buy2MeItem.getUseravatar(), mContext);
            mItemName2.setText(buy2MeItem.getName());
            mItemSeller.setText(buy2MeItem.getDeliver_to());
            mItemExtras.setText(buy2MeItem.getPrice());
        }

    }

    public void addAll(List<Buy2MeItem> models) {
        mProfileArrayList.addAll(models);
        notifyDataSetChanged();
    }


    public Buy2MeListRVAdapter(Context context, ArrayList<Buy2MeItem> profileArrayList) {
        this.mProfileArrayList = profileArrayList;
        this.mContext = context;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ship_shop_item, viewGroup, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder viewHolder, final int i) {
        final Buy2MeItem buy2MeItem = mProfileArrayList.get(i);
        viewHolder.bindItem(buy2MeItem);
    }


    private int getColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public int getItemCount() {
        return mProfileArrayList.size();
    }
}
