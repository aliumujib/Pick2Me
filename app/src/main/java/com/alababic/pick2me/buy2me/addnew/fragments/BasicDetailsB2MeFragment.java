package com.alababic.pick2me.buy2me.addnew.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.ImagePickerListener;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.utils.commons.Constants;
import com.desmond.squarecamera.CameraActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.reactivex.functions.Consumer;

import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

/**
 * Created by ABDUL-MUJEEB ALIU on 26/11/2017.
 */

@SuppressLint("ValidFragment")
public class BasicDetailsB2MeFragment extends Fragment {

    private LinearLayout mAddLogoBtnLay;
    private EditText mItemName;
    private EditText mItemPrice;
    private EditText mDateStart;
    private EditText mDeadlineDate;

    private Calendar mSharedCalendar = Calendar.getInstance();


    DatePickerDialog.OnDateSetListener mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            mSharedCalendar.set(YEAR, year);
            mSharedCalendar.set(MONTH, monthOfYear);
            mSharedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String date = getDateString(mSharedCalendar);
            mDeadlineDate.setText(date);
            System.out.println(date);
        }

    };
    private ImagePickerListener mListener;
    private ImageView mItemImage;

    private String getDateString(Calendar date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date.getTime().getTime());
    }

    public BasicDetailsB2MeFragment() {

    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.shipping_info_fragment, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ImagePickerListener){
            mListener = (ImagePickerListener) context;
        }
    }

    @Override
    public void onViewCreated(View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        mAddLogoBtnLay = rootView.findViewById(R.id.add_logo_btn_lay);
        mItemName = rootView.findViewById(R.id.item_name);
        mItemImage = rootView.findViewById(R.id.item_image);
        mItemPrice = rootView.findViewById(R.id.item_price);
        mDateStart = rootView.findViewById(R.id.date_start);
        mDeadlineDate = rootView.findViewById(R.id.deadline_date);

        mDateStart.setText(getDateString(mSharedCalendar));

        mAddLogoBtnLay.setOnClickListener(v -> {
            // Start CameraActivity
            Intent startCustomCameraIntent = new Intent(getContext(), CameraActivity.class);
            getActivity().startActivityForResult(startCustomCameraIntent, Constants.REQUEST_CAMERA_MAIN);
        });


        mDeadlineDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();

                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        mOnDateSetListener,
                        now.get(YEAR),
                        now.get(MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                final android.app.FragmentManager manager = ((Activity) getActivity()).getFragmentManager();

                dpd.show(manager, "Datepickerdialog");
            }
        });

        mListener.getImagePickerURIObservable().subscribe(new Consumer<Uri>() {
            @Override
            public void accept(Uri uri) throws Exception {
                Utils.loadImageFromSd(mItemImage, uri.getPath(), getContext());
            }
        });
    }
}