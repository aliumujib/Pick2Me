package com.alababic.pick2me.buy2me.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic_app.pick2me_data.models.ProfileItem;

/**
 * Created by abdulmujibaliu on 3/10/17.
 */

public class MessageDetailDiagFragment extends DialogFragment {
    private Context mContext;
    private String TAG = getClass().getSimpleName();
    private TextView mReciepientTextView, mSenderFullNameTextView, mEmailTextView, mMobileNumberTextView, mTypeoFEnquiryTextView, mMessageTextView;


    public static MessageDetailDiagFragment newInstance(ProfileItem profileItem) {
        MessageDetailDiagFragment fragment = new MessageDetailDiagFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private void initViews(){

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.message_detail_diag);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        mReciepientTextView = (TextView) dialog.findViewById(R.id.reciepient);
        mSenderFullNameTextView = (TextView) dialog.findViewById(R.id.user_user_name);
        mEmailTextView = (TextView) dialog.findViewById(R.id.user_email);
        mMobileNumberTextView = (TextView) dialog.findViewById(R.id.user_phone);
        mTypeoFEnquiryTextView = (TextView) dialog.findViewById(R.id.media_inquiry_type);
        mMessageTextView = (TextView) dialog.findViewById(R.id.message_text);
        mMessageTextView.setMovementMethod(new ScrollingMovementMethod());

        return dialog;
    }


    @Override
    public void onResume() {
        super.onResume();

        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayoutCompat.LayoutParams.WRAP_CONTENT;
        params.height = LinearLayoutCompat.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setGravity(Gravity.CENTER_VERTICAL);
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

    }
}
