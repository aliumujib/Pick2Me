package com.alababic.pick2me.buy2me.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.R;
import com.alababic.pick2me.buy2me.adapters.Buy2MeListRVAdapter;
import com.alababic.pick2me.buy2me.addnew.AddNewBuy2MeActivity;
import com.alababic_app.pick2me_data.models.Buy2MeItem;
import com.alababic_app.pick2me_data.repositories.Buy2MeRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by abdulmujibaliu on 3/9/17.
 */

public class Buy2MeListFragment extends Fragment {


    private final static String TAG = "InboxFragment";
    private RecyclerView mRecyclerView;
    public Buy2MeListRVAdapter mProfileListRVAdapter;
    public ArrayList<Buy2MeItem> mProfileArrayList = new ArrayList<>();

    RecyclerView rv;
    FloatingActionButton addRecordFab;


    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_buy_2_me, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv);
        GridLayoutManager llm = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true); // to improve performance
        setHasOptionsMenu(true);
        mProfileListRVAdapter = new Buy2MeListRVAdapter(getContext(), mProfileArrayList);
        mRecyclerView.setAdapter(mProfileListRVAdapter);

        this.addRecordFab = view.findViewById(R.id.add_record_fab);

        //THIS IS TERRIBLEEEEEEEE
        //TODO ADDD PRESENTER AND NAVIGATOR

        addRecordFab.setOnClickListener(v -> AddNewBuy2MeActivity.startActivity(getContext()));

        Buy2MeRepository.sharedInstance.getBuy2MeListData().subscribe(buy2MeItems -> mProfileListRVAdapter.addAll(buy2MeItems), Throwable::printStackTrace);

        return view;
    }



    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static Fragment newInstance(int sectionNumber) {
        Fragment fragment = new Buy2MeListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

}
