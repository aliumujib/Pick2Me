/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/7/17 12:32 AM
 */

package com.alababic.pick2me.buy2me.addnew;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.ImagePickerListener;
import com.alababic.pick2me.utils.commons.Constants;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public class AddNewBuy2MeActivity extends AppCompatActivity implements ImagePickerListener {

    private PublishSubject<Uri> mImagePickerURISubject  = PublishSubject.create();

    public static void startActivity(Context context){
        Intent intent = new Intent(context, AddNewBuy2MeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_b2m_flow);
    }

    // Receive Uri of saved square photo
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == Constants.REQUEST_CAMERA_MAIN) {
            Uri photoUri = data.getData();
            mImagePickerURISubject.onNext(photoUri);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public Observable<Uri> getImagePickerURIObservable() {
        return mImagePickerURISubject;
    }
}
