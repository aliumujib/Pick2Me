package com.alababic.pick2me.buy2me;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.content.Context;

import com.alababic.pick2me.R;
import com.alababic.pick2me.base.BaseActivity;
import com.alababic.pick2me.buy2me.fragments.Buy2MeListFragment;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.utils.commons.Constants;

public class Buy2MeActivity extends BaseActivity {


    private ActionBar mActionBar;

    public static void start(Context context) {
        Intent intent = new Intent(context, Buy2MeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setDisplayShowTitleEnabled(true);
        }

        Utils.replaceFragmentWithorWithoutBackState(Buy2MeListFragment.newInstance(2), this, Constants.WITHOUTBSTATE, R.id.container);
        setActionBarTitle("Buy2Me List");

    }

    void setActionBarTitle(String title){
        mActionBar.setTitle(title);
    }


}
