package com.alababic.pick2me.viewrecourse;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.alababic.pick2me.R;
import com.github.barteksc.pdfviewer.PDFView;

public class RecourseViewerActivity extends AppCompatActivity {

    public static void start(Context context, String recourseName) {
        Intent intent = new Intent(context, RecourseViewerActivity.class);
        intent.putExtra("recourse_name", recourseName);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recourse_viewer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getIntent().getStringExtra("recourse_name"));
        }
        PDFView mDPdfView = (PDFView) findViewById(R.id.pdfView);
        mDPdfView.fromAsset("bundle_pdf.pdf").load();
        mDPdfView.enableSwipe(true);
        //mDPdfView.loadPages();
    }

}
