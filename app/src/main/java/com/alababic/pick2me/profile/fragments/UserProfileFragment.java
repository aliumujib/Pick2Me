package com.alababic.pick2me.profile.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.base.BaseFragment;
import com.alababic_app.pick2me_data.models.User;
import com.alababic_app.pick2me_data.repositories.UserRepository;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Notification;
import io.reactivex.functions.Consumer;


/**
 * Created by Abdul-Mujeeb Aliu on 7/23/2016.
 */
public class UserProfileFragment extends BaseFragment {

    View mCardShadow, mSearchCard;
    private CircleImageView mUserProfileProfileImage;
    private TextView mUserName;
    private TextView mUserEmail;
    private TextView mUserPhone;


    @Override
    public void onResume() {
        super.onResume();

        mCardShadow.setVisibility(View.GONE);
        mSearchCard.setVisibility(View.GONE);
    }


    @Override
    public void onPause() {
        super.onPause();

        mCardShadow.setVisibility(View.VISIBLE);
        mSearchCard.setVisibility(View.VISIBLE);
    }

    public UserProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_view, container, false);
        initView(view);
        return view;
    }

    public static UserProfileFragment newInstance() {

        Bundle args = new Bundle();

        UserProfileFragment fragment = new UserProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void initView(View convertView) {
        mCardShadow = getActivity().findViewById(R.id.card_shadow);
        mSearchCard = getActivity().findViewById(R.id.search_card_view);
        this.mUserProfileProfileImage = convertView.findViewById(R.id.user_profile_profile_image);
        this.mUserName = convertView.findViewById(R.id.user_name);
        this.mUserEmail = convertView.findViewById(R.id.user_email);
        this.mUserPhone = convertView.findViewById(R.id.user_phone);

        UserRepository.sharedInstance.getCurrentUser().subscribe(new Consumer<Notification<User>>() {
            @Override
            public void accept(Notification<User> userNotification) throws Exception {
             if(userNotification.isOnError()){
                 handleFail(userNotification.getError());
             }else {
                 User user = userNotification.getValue();
                 mUserName.setText(user.getUsername());
                 mUserEmail.setText(user.getEmail());
                 mUserPhone.setText("");
             }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {

            }
        });
    }
}
