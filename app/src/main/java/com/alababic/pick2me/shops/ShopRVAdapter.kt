package com.alababic.pick2me.shops

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.alababic.pick2me.R
import com.alababic.pick2me.utils.Utils
import com.alababic_app.pick2me_data.models.Shop

/**
 * Created by abdulmujibaliu on 10/22/17.
 */


open class ShopRVAdapter(private val mContext: Context, mainPageResponseList: MutableList<Shop>,
                         shopItemClickListener: OnShopItemClickListener) : RecyclerView.Adapter<ShopRVAdapter.ViewHolder>() {
    private val inflater: LayoutInflater
    private var mainPageResponseList = mutableListOf<Shop>()
    private val mType: String? = null
    private val shopItemClickListener: OnShopItemClickListener? = shopItemClickListener


    init {
        inflater = LayoutInflater.from(mContext)
        this.mainPageResponseList = mainPageResponseList
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val root = inflater.inflate(R.layout.item_shop, parent, false)

        return ViewHolder(root)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val productItem = mainPageResponseList[position]
        viewHolder.bindItem(productItem, mContext)
    }


    override fun getItemCount(): Int {
        return mainPageResponseList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageView: ImageView
        var shopname: TextView

        init {
            this.imageView = view.findViewById<ImageView>(R.id.shop_image)
            this.shopname = view.findViewById<TextView>(R.id.shop_name) as TextView
        }

        fun bindItem(shop: Shop, context: Context) {
            Utils.loadImage(this.imageView, shop.imageURL, context)
            this.shopname.text = shop.name
            this.itemView.setOnClickListener {
                shopItemClickListener?.onShopItemClickListener(shop)
            }
        }

    }
}