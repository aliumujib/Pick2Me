package com.alababic.pick2me.shops

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.alababic.pick2me.R
import com.alababic.pick2me.utils.commons.Constants
import com.alababic_app.pick2me_data.models.Category

import kotlinx.android.synthetic.main.activity_shops.*

class ShopsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shops)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        var category = intent.extras.get(Constants.CATEG_INTENT_EXTRA) as Category
        supportActionBar?.title = category.categName
    }


    companion object {

        @JvmStatic
        fun start(context: Context, category: Category) {
            val intent = Intent(context, ShopsActivity::class.java)
            intent.putExtra(Constants.CATEG_INTENT_EXTRA, category)
            context.startActivity(intent)
        }
    }

}
