package com.alababic.pick2me.shops

import com.alababic_app.pick2me_data.models.Shop

/**
 * Created by abdulmujibaliu on 10/22/17.
 */
interface OnShopItemClickListener {

    fun onShopItemClickListener(shop: Shop)

}