package com.alababic.pick2me.shops

import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alababic.pick2me.R
import com.alababic.pick2me.shop_details.ShopDetailsActivity
import com.alababic.pick2me.utils.ToastUtils
import com.alababic_app.pick2me_data.models.Shop
import com.alababic_app.pick2me_data.repositories.ShopRepository

/**
 * A placeholder fragment containing a simple view.
 */
class ShopsActivityFragment : Fragment(), OnShopItemClickListener {

    override fun onShopItemClickListener(shop: Shop) {
        ShopDetailsActivity.start(context, shop)
    }

    var shopRVAdapter: ShopRVAdapter? = null
    var shopRV: RecyclerView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_shops, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        shopRV = view?.findViewById<RecyclerView>(R.id.recyclerView) as RecyclerView
        shopRV?.layoutManager = LinearLayoutManager(context)

        ShopRepository.sharedInstance.shopData.subscribe({ data ->
            shopRVAdapter = ShopRVAdapter(context, data, this)
            shopRV?.adapter = shopRVAdapter
            shopRVAdapter?.notifyDataSetChanged()
        }, { throwable ->
            throwable.printStackTrace()
            ToastUtils.longToast(throwable.localizedMessage)
        })

    }


    companion object {

        @JvmStatic
        fun newInstance(): ShopsActivityFragment {
            var shopActivityFrag = ShopsActivityFragment();
            return shopActivityFrag
        }
    }
}
