/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 3/27/17 1:41 AM
 */

package com.alababic.pick2me.productdetails.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.productdetails.interfaces.ProductDetailView;
import com.alababic.pick2me.utils.views.QtyCounterView;
import com.alababic.pick2me.utils.views.thumbnailpickerview.ThumbnailPickerView;
import com.alababic_app.pick2me_data.models.ProductItem;
import com.alababic_app.pick2me_data.repositories.CartRepository;

/**
 * Created by abdulmujibaliu on 3/8/17.
 */

public abstract class BaseItemDetailsFragment extends Fragment implements QtyCounterView.QtyChangeListener {

    private View mRootView;
    private NestedScrollView mScroll;
    private TextView mProductName;
    private TextView mProductPrice;
    private TextView mProductSellerName;
    private TextView mLocationDetails;

    protected ProductDetailView mProductDetailView;
    protected ThumbnailPickerView mThumbNailPicker;
    private QtyCounterView mQtyCounterView;
    private ProductItem mProductItem;

    @Override
    public void onQtyChnaged(int newQty) {
        CartRepository.sharedInstance.addItemToCart(mProductItem);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutResID(), container, false);


        return mRootView;
    }


    @LayoutRes
    abstract int getLayoutResID();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);

        mProductDetailView.getProductDetailsDetails().subscribe(productItem -> initProduct(productItem));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ProductDetailView) {
            mProductDetailView = (ProductDetailView) context;
        }
    }


    protected void initView(View convertView) {
        this.mScroll = (NestedScrollView) convertView.findViewById(R.id.scroll);
        this.mProductName = (TextView) convertView.findViewById(R.id.product_name);
        this.mProductPrice = (TextView) convertView.findViewById(R.id.product_price);
        this.mProductSellerName = (TextView) convertView.findViewById(R.id.product_seller_name);
        this.mLocationDetails = (TextView) convertView.findViewById(R.id.location_details);
        this.mThumbNailPicker = (ThumbnailPickerView) convertView.findViewById(R.id.thumbnails_picker);
        this.mQtyCounterView = (QtyCounterView) convertView.findViewById(R.id.qty_counter_view);
        this.mThumbNailPicker.setOnThumbNailsClickedListener(mProductDetailView);
        this.mQtyCounterView.setOnQtyChangedListener(this);
    }


    protected void initProduct(ProductItem productItem) {
        mProductItem = productItem;
        mProductName.setText(productItem.getmItemName());
        mProductSellerName.setText(productItem.getmSellerName());
        mProductPrice.setText(productItem.getmProductPriceString());
        if(mLocationDetails!=null){
            this.mLocationDetails.setText("NOT AVAILABLE");
        }
    }
}
