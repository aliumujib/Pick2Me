/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/22/17 1:23 PM
 */

package com.alababic.pick2me.productdetails.interfaces;

import com.alababic_app.pick2me_data.models.ProductItem;

import io.reactivex.Observable;


/**
 * Created by abdulmujibaliu on 3/7/17.
 */

public interface ProductDetailView extends OnThumbnailsClickedListener{

    Observable<ProductItem> getProductDetailsDetails();

}
