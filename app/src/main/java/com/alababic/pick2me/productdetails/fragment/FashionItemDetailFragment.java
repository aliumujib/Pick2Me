package com.alababic.pick2me.productdetails.fragment;

import android.os.Bundle;
import android.view.View;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.views.colorpickerview.ColorPickerView;
import com.alababic_app.pick2me_data.models.ProductItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdulmujibaliu on 11/13/17.
 */

public class FashionItemDetailFragment extends BaseItemDetailsFragment{
    private ColorPickerView mColorPicker;

    @Override
    int getLayoutResID() {
        return R.layout.fragment_fashion_item_detail;
    }

    public static FashionItemDetailFragment newInstance() {
        
        Bundle args = new Bundle();
        
        FashionItemDetailFragment fragment = new FashionItemDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void initProduct(ProductItem productItem) {
        super.initProduct(productItem);

        List<String> imaStrings = new ArrayList<>();
        imaStrings.add("http://www.nourishinteractive.com/system/assets/general/images/foods/plate-of-mixed-dried-bean.gif");
        imaStrings.add("http://www.nourishinteractive.com/system/assets/general/images/foods/legumes-foods-protein-meals.png");
        imaStrings.add("http://www.nourishinteractive.com/system/assets/general/images/foods/legumes-foods-children-protein-meals.png");
        imaStrings.add("http://nutritionmyths.com/wp-content/uploads/2015/08/P0329_beans_legumes_56687253.jpg");


        List<String> colorStrings = new ArrayList<>();
        colorStrings.add("#4FC3F7");
        colorStrings.add("#b71c1c");
        colorStrings.add("#8C9EFF");
        colorStrings.add("#ff1744");


        mThumbNailPicker.setImages(imaStrings);
        mColorPicker.setColors(colorStrings);

    }

    @Override
    protected void initView(View convertView) {
        super.initView(convertView);

        mColorPicker = (ColorPickerView) convertView.findViewById(R.id.color_picker);
    }
}
