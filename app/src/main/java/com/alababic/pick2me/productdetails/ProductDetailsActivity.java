package com.alababic.pick2me.productdetails;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.productdetails.adapter.ViewPagerAdapter;
import com.alababic.pick2me.productdetails.interfaces.ProductDetailView;
import com.alababic.pick2me.utils.PicassoCache;
import com.alababic.pick2me.utils.commons.Constants;
import com.alababic.pick2me.productdetails.libs.AppBarStateChangeListener;
import com.alababic.pick2me.productdetails.libs.Utils;
import com.alababic_app.pick2me_data.repositories.CartRepository;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.alababic_app.pick2me_data.models.ProductItem;
import com.rowland.cartcounter.view.CartCounterActionView;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.ReplaySubject;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailView {

    private Toolbar mToolbar;
    private CollapsingToolbarLayout mCollapsingToolbar;
    private AppBarLayout mAppBarLayout;
    private TabLayout mTabLayout;
    private TextView mToolbarTextView;
    private KenBurnsView mHeaderImageView;
    private View mContainerView;
    private ViewPager mViewPager;
    private ViewPagerAdapter mAdapter;
    private TextView mJournalTitleTextView, mPublisherNameTextView, mPublishedPaperCountTextView;
    private ProductItem mProductItem;
    public EditText mQtyEditText;

    private ReplaySubject<ProductItem> mProductItemPublishSubject = ReplaySubject.create();
    private CartCounterActionView mCartCounterActionView;

    public static void start(Context context, ProductItem productItem) {
        Intent intent = new Intent(context, ProductDetailsActivity.class);
        intent.putExtra(Constants.RECOURSE_ITEM_EXTRA, productItem);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal_detail);

        if ((mProductItem = getIntent().getExtras().getParcelable(Constants.RECOURSE_ITEM_EXTRA)) != null) {
            findViews();
            setUpViews(mProductItem);
            mProductItemPublishSubject.onNext(mProductItem);
        } else {
            //TODO Show error somehow
        }

    }

    private void findViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbarTextView = (TextView) findViewById(R.id.toolbar_title);
        mCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mHeaderImageView = (KenBurnsView) findViewById(R.id.imageView_header);
        mContainerView = findViewById(R.id.view_container);
        mPublisherNameTextView = (TextView) findViewById(R.id.author_name);
        mPublishedPaperCountTextView = (TextView) findViewById(R.id.year_of_publication);
        mJournalTitleTextView = (TextView) findViewById(R.id.recourcesname);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mHeaderImageView != null) {
            mHeaderImageView.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mHeaderImageView != null) {
            mHeaderImageView.pause();
        }
    }

    private void setUpViews(ProductItem mProductItem) {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        mCollapsingToolbar.setTitleEnabled(false);
        mCollapsingToolbar.getLayoutParams().height =
                (Utils.isLand(this) ? Utils.getDisplayDimen(this).y :
                        Utils.getDisplayDimen(this).x) * 9 / 16 +
                        Utils.getStatusBarHeightPixel(this);
        mCollapsingToolbar.requestLayout();

        // TODO : Hack for CollapsingToolbarLayout
        mToolbarTextView.setText(this.mProductItem.getmItemName());
        actionBarResponsive();
        mAppBarLayout.addOnOffsetChangedListener(mAppBarStateChangeListener);

        mAdapter = new ViewPagerAdapter(getSupportFragmentManager(), mProductItem);
        mViewPager.setOffscreenPageLimit(mAdapter.getCount());
        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        PicassoCache.getPicassoInstance(getApplication()).load(this.mProductItem.getmImageUrl()).fit().centerCrop().into(mHeaderImageView);
        mJournalTitleTextView.setText(this.mProductItem.getmItemName());
        mPublisherNameTextView.setText(this.mProductItem.getmSellerName());
        mPublishedPaperCountTextView.setText(this.mProductItem.getmProductPriceString() + " per kg");

        // If your CollapsingToolbarLayout too complex (e.g. ImageView into FrameLayout), then
        // your status bar may looks so buggy.
        // You can hotfix by this code when you need to use some 24.2.0's features,
        // or you can wait for Google fix this (24.2.1), or downgrade to 24.1.1.
        // The issue: http://goo.gl/FMWs37
        ViewCompat
                .setOnApplyWindowInsetsListener(mContainerView, new OnApplyWindowInsetsListener() {

                    @Override
                    public WindowInsetsCompat onApplyWindowInsets(View v,
                                                                  WindowInsetsCompat insets) {
                        return insets.consumeSystemWindowInsets();
                    }
                });

    }

    private void setAlphaForView(View v, float alpha) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(alpha, alpha);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private void actionBarResponsive() {
        int actionBarHeight = Utils.getActionBarHeightPixel(this);
        int tabHeight = Utils.getTabHeight(this);
        if (actionBarHeight > 0) {
            mToolbar.getLayoutParams().height = actionBarHeight + tabHeight;
            mToolbar.requestLayout();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_product_details, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.menu_cart_add:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message

                addToCart();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem cartItem = menu.findItem(R.id.menu_cart_add);
        mCartCounterActionView = (CartCounterActionView) cartItem.getActionView();

        CartRepository.sharedInstance.getCartCount().subscribe(integer -> mCartCounterActionView.setCount(integer));

        return super.onPrepareOptionsMenu(menu);
    }

    private void addToCart() {
//        CartItemEntityManager cartItemEntityManager = new CartItemEntityManager();
//        CartItem cartI = new CartItem(mProductItem, Integer.parseInt(mQtyEditText.getText().toString()));
//        cartItemEntityManager.add(cartI);
//        ToastUtils.shortToast("Added Item to cart!");
    }

    AppBarStateChangeListener mAppBarStateChangeListener = new AppBarStateChangeListener() {

        @Override
        public void onStateChanged(AppBarLayout appBarLayout, State state) {
            if (mToolbarTextView != null) {
                if (state == State.COLLAPSED) {
                    if (Build.VERSION.SDK_INT >= 11) {
                        mToolbarTextView.setAlpha(1);
                    } else {
                        setAlphaForView(mToolbarTextView, 1);
                    }
                    mCollapsingToolbar.setContentScrimColor(
                            ContextCompat.getColor(ProductDetailsActivity.this, R.color.colorPrimary));
                } else if (state == State.EXPANDED) {
                    if (Build.VERSION.SDK_INT >= 11) {
                        mToolbarTextView.setAlpha(0);
                    } else {
                        setAlphaForView(mToolbarTextView, 0);
                    }
                    mCollapsingToolbar.setContentScrimColor(ContextCompat
                            .getColor(ProductDetailsActivity.this, android.R.color.transparent));
                }
            }
        }

        @Override
        public void onOffsetChanged(State state, float offset) {
            if (mToolbarTextView != null) {
                if (state == State.IDLE) {
                    if (Build.VERSION.SDK_INT >= 11) {
                        mToolbarTextView.setAlpha(offset);
                        mCollapsingToolbar.setContentScrimColor(
                                (int) new android.animation.ArgbEvaluator().evaluate(offset,
                                        ContextCompat.getColor(ProductDetailsActivity.this,
                                                android.R.color.transparent), ContextCompat
                                                .getColor(ProductDetailsActivity.this,
                                                        R.color.colorPrimary)));
                    } else {
                        setAlphaForView(mToolbarTextView, offset);
                        mCollapsingToolbar.setContentScrimColor((int) new ArgbEvaluator()
                                .evaluate(offset, ContextCompat.getColor(ProductDetailsActivity.this,
                                        android.R.color.transparent), ContextCompat
                                        .getColor(ProductDetailsActivity.this, R.color.colorPrimary)));
                    }
                }
            }
        }
    };


    @Override
    public Observable<ProductItem> getProductDetailsDetails() {
        return mProductItemPublishSubject;
    }

    @Override
    public void thumbNailClicked(String imageURL) {
        PicassoCache.getPicassoInstance(getApplication()).load(imageURL).fit().centerCrop().into(mHeaderImageView);
    }
}
