package com.alababic.pick2me.productdetails.adapter;

/**
 * Created by abdulmujibaliu on 11/12/17.
 * <p>
 * the menu layout has the 'add/new' menu item
 * <p>
 * the menu layout has the 'add/new' menu item
 * <p>
 * the menu layout has the 'add/new' menu item
 */


/**
 * the menu layout has the 'add/new' menu item
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.alababic.pick2me.productdetails.fragment.DescLinesFragment;
import com.alababic.pick2me.productdetails.fragment.BaseItemDetailsFragment;
import com.alababic.pick2me.productdetails.fragment.FashionItemDetailFragment;
import com.alababic.pick2me.productdetails.fragment.QtyItemDetailFragment;
import com.alababic.pick2me.productdetails.fragment.ReviewListFragment;
import com.alababic_app.pick2me_data.models.ProductCategoryType;
import com.alababic_app.pick2me_data.models.ProductItem;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    private final ProductItem mProductItem;
    private String[] titles = {"ITEM DETAILS", "ITEM DESC", "REVIEWS"};

    public ViewPagerAdapter(FragmentManager fragmentManager, ProductItem mProductItem) {
        super(fragmentManager);
        this.mProductItem = mProductItem;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return getDetailFragment();
            case 1:
                return DescLinesFragment.newInstance();
            case 2:
                return ReviewListFragment.newInstance();
            default:
                return QtyItemDetailFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    public Fragment getDetailFragment() {

        if (mProductItem.getmProductCategoryType() == ProductCategoryType.TYPE_FOOD.ordinal()) {
            return QtyItemDetailFragment.newInstance();
        } else   {
            return FashionItemDetailFragment.newInstance();
        }

        //(mProductItem.getmProductCategoryType() == ProductCategoryType.TYPE_FASHION)
    }
}
