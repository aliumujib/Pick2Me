/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/22/17 1:23 PM
 */

package com.alababic.pick2me.productdetails.interfaces;

/**
 * Created by abdulmujibaliu on 3/7/17.
 */

public interface OnThumbnailsClickedListener {

    void thumbNailClicked(String imageURL);

}
