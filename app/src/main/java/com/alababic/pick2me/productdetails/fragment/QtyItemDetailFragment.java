package com.alababic.pick2me.productdetails.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic_app.pick2me_data.models.ProductItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by abdulmujibaliu on 11/12/17.
 */

public class QtyItemDetailFragment extends BaseItemDetailsFragment {
    private TextView mBestBefore;

    public static BaseItemDetailsFragment newInstance() {
        Bundle args = new Bundle();
        BaseItemDetailsFragment fragment = new QtyItemDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    protected void initProduct(ProductItem productItem) {
        super.initProduct(productItem);
        mBestBefore.setText(productItem.getmProductDateOfExpiryYear());

        List<String> imaStrings = new ArrayList<>();
        imaStrings.add("http://www.nourishinteractive.com/system/assets/general/images/foods/plate-of-mixed-dried-bean.gif");
        imaStrings.add("http://www.nourishinteractive.com/system/assets/general/images/foods/legumes-foods-protein-meals.png");
        imaStrings.add("http://www.nourishinteractive.com/system/assets/general/images/foods/legumes-foods-children-protein-meals.png");
        imaStrings.add("http://nutritionmyths.com/wp-content/uploads/2015/08/P0329_beans_legumes_56687253.jpg");

        mThumbNailPicker.setImages(imaStrings);
    }

    @Override
    protected void initView(View convertView) {
        super.initView(convertView);
        mBestBefore = convertView.findViewById(R.id.best_before);
    }

    @Override
    int getLayoutResID() {
        return R.layout.fragment_detail_qty_item;
    }
}
