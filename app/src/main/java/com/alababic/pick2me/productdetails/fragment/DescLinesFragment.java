package com.alababic.pick2me.productdetails.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.productdetails.interfaces.OnThumbnailsClickedListener;

/**
 * Created by abdulmujibaliu on 3/8/17.
 */

public class DescLinesFragment extends Fragment {

    View mRootView;
    Context mContext;
    private TextView mDescGuideLinesTextView;

    public static Fragment newInstance(){
        return  new DescLinesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_sub_guidelines, container, false);

        findViews();

        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    private void findViews() {
        mDescGuideLinesTextView = (TextView) mRootView.findViewById(R.id.submission_guidelines_text_view);
    }

    private void setUpViews() {

    }


}