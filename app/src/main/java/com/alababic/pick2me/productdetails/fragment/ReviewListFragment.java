package com.alababic.pick2me.productdetails.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic_app.pick2me_data.models.ProductItem;
import com.alababic.pick2me.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReviewListFragment extends Fragment {

	private	View mRootView;
	private	RecyclerView mRecyclerView;
	private	Context mContext;
	private ArrayList<ProductItem> mProductList = new ArrayList<>();
	private PapersAdapter mAdapter;

	public static Fragment newInstance(){
		return  new ReviewListFragment();
	}


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_paper_list, container, false);

		findViews();
		//initData();
		setUpViews();

		return mRootView;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mContext = context;
	}


	private void findViews() {
		mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recyclerView);
	}

	private void setUpViews() {
		mAdapter = new PapersAdapter(getContext(), mProductList);
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setItemAnimator(new DefaultItemAnimator());
		mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
		mRecyclerView.setAdapter(mAdapter);
	}


/*
	private void initData() {
		mProductList.clear();

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1481724629_5436.jpg", "West African Journal of Pharmacy", "Dr Ibrahim Oreagba", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480417255_5897.jpg", "Journal of Science and Practice of Pharmacy", "Dr. U. V. Odili", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480418332_1068.jpg", "Journal of Geographic Thought & Environmental Studies", "Prof S. B. Arokoyu" , "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1478261396_6875.jpg", "Nigerian Journal of Microbiology", "Prof. J. N. Ogbulie", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1481724629_5436.jpg", "West African Journal of Pharmacy", "Dr Ibrahim Oreagba" , "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480417255_5897.jpg", "Journal of Science and Practice of Pharmacy", "Dr. U. V. Odili", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480418332_1068.jpg", "Journal of Geographic Thought & Environmental Studies", "Prof S. B. Arokoyu", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1478261396_6875.jpg", "Nigerian Journal of Microbiology", "Prof. J. N. Ogbulie" , "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1481724629_5436.jpg", "West African Journal of Pharmacy", "Dr Ibrahim Oreagba", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480417255_5897.jpg", "Journal of Science and Practice of Pharmacy", "Dr. U. V. Odili", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480418332_1068.jpg", "Journal of Geographic Thought & Environmental Studies", "Prof S. B. Arokoyu" , "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1478261396_6875.jpg", "Nigerian Journal of Microbiology", "Prof. J. N. Ogbulie", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1481724629_5436.jpg", "West African Journal of Pharmacy", "Dr Ibrahim Oreagba" , "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480417255_5897.jpg", "Journal of Science and Practice of Pharmacy", "Dr. U. V. Odili", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1480418332_1068.jpg", "Journal of Geographic Thought & Environmental Studies", "Prof S. B. Arokoyu", "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));

		mProductList.add(new RecourseItem("http://www.academix.ng/documents/journals/1478261396_6875.jpg", "Nigerian Journal of Microbiology", "Prof. J. N. Ogbulie" , "Pharmacy and Pharmaceutical Sciences",
				"Medicine and Health Sciences", "West African Post-Graduate College", "2013", "Ismail A. Suleiman\n" +
				"Fola Tayo", "Nigeria", "Background: There is the need for improved efficiency and minimization in cost of antibacterial therapy in developing countries. The study objective was to carry out cost minimization analysis of antibacterial in a tertiary health care facility in Nigeria between the year 2005 and 2006.\n" +
				"Methods: Drug utilization review was carried out using prescriptions in 525 consecutively sampled case notes of some infectious diseases retrospectively. Relevant data such as demographics, diagnosis, prescribed drugs, and dosages were extracted. Cost per defined daily dose of each drug, the cost of each prescription and the average cost of antibacterial agents per patient were determined. This was followed by economic evaluation using Cost Minimization Analysis. Data were analysed appropriately.\n" +
				"Results: Amoxicillin and coamoxiclav for ENT infections and ciprofloxacin/doxycycline for STIs were the most widely utilized. Generic products, higher strength, solid dosage forms and oral formulations are more cost effective than branded agents, lower strength, liquid formulations and injections of the same drug entity respectively. For instance, the use of generic product of ciprofloxacin for 5 days for 1000 patients (5000DDDs) resulted in a cost saving of NGN1.04million (USD9,333.33).\n" +
				"\u200BConclusion: Only few drug items were predominantly responsible for high antibacterial therapy costs which need to be closely monitored for cost to be appreciably minimized. Generic antibacterial agents, higher strength preparations of the same drugs, solid forms in preference to syrups among school age children and oral agents in preference for injection should be used. Significant cost could be minimised and improved access to essential drugs assured if these are followed."));
	}
*/


	public class PapersAdapter extends RecyclerView.Adapter<PapersAdapter.ViewHolder> {
		private final LayoutInflater inflater;
		private List<ProductItem> mainPageResponseList = new ArrayList<>();
		private Context mContext;
		public PapersAdapter(Context context, List<ProductItem> mainPageResponseList) {
			inflater = LayoutInflater.from(context);
			this.mainPageResponseList = mainPageResponseList;
			this.mContext = context;
		}
		public PapersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
														   int viewType) {
			View root = inflater.inflate(R.layout.item_view_auto, parent, false);
			ViewHolder viewHolder = new ViewHolder(root);
			return viewHolder;
		}
		public void onBindViewHolder(final ViewHolder viewHolder, int position) {
			final ProductItem object = mainPageResponseList.get(position);
			Utils.loadImage(viewHolder.imageView, object.getmImageUrl(), mContext);
			viewHolder.packageName.setText(object.getmItemName());
			viewHolder.packageSeller.setText(object.getmSellerName());
			viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					//RecourseDetailActivity.start(viewHolder.imageView.getContext(), object);
				}
			});
		}
		public final class ViewHolder extends RecyclerView.ViewHolder {
			protected ImageView imageView;
			protected TextView packageName;
			protected TextView packageSeller;
			public ViewHolder(View view) {
				super(view);
				this.imageView = (ImageView) view.findViewById(R.id.item_icon);
				this.packageName = (TextView) view.findViewById(R.id.item_title);
				this.packageSeller = (TextView) view.findViewById(R.id.item_seller);
			}
		}
		public int getItemCount() {
			return mainPageResponseList.size();
		}
	}


}
