/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 3/27/17 2:38 AM
 */

package com.alababic.pick2me.cart.fragment;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alababic_app.pick2me_data.models.CartItemEntityManager;
import com.github.glomadrian.materialanimatedswitch.MaterialAnimatedSwitch;
import com.alababic.pick2me.R;
import com.alababic.pick2me.buy2me.addnew.AddNewBuy2MeActivity;
import com.alababic.pick2me.productdetails.ProductDetailsActivity;
import com.alababic.pick2me.utils.Utils;
import com.alababic_app.pick2me_data.models.CartItem;

import java.util.ArrayList;
import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */

public class CartActivityFragment extends Fragment {


    private View mRootView;
    private RecyclerView mRecyclerView;
    private TextView mSubtotal;
    private TextView mTotal;
    private Button mCheckOutButton;
    private Button mKeepBuyingButton; //Dear customers... don't ever click this button, it is a setup! I know you think it is a good
    // idea to keep clicking ... but dont if not these people will take all your money but then you get good food soooooooo win - win.

    private Context mContext;
    private List<CartItem> mCartItemList = new ArrayList<>();
    private PapersAdapter mAdapter;

    final int[] totalPrice = {0};


    public CartActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_cart, container, false);
        mSubtotal = (TextView) mRootView.findViewById(R.id.sub_total);
        mTotal = (TextView) mRootView.findViewById(R.id.total);

        mContext = getContext();

        findViews();
        setUpViews();

        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    private void findViews() {
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recyclerView);
        mCheckOutButton = (Button) mRootView.findViewById(R.id.proceed_to_check_out);
        mKeepBuyingButton = (Button) mRootView.findViewById(R.id.back_to_shopping);
    }

    private void setUpViews() {
        mAdapter = new PapersAdapter(getContext(), mCartItemList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);

        //SETUP THEM BUTTONS
        mCheckOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNewBuy2MeActivity.startActivity(getContext());
            }
        });

        mKeepBuyingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }


    private void initData() {
        //mCartItemList.clear();
        mCartItemList.addAll(new CartItemEntityManager().select()
                .asList());
        Log.d("PRODLIST", "" + mCartItemList.size());

        //mAdapter.notifyDataSetChanged(); //TODO not working? WTF

//        for (int i = 0; i < mCartItemList.size(); i++) {
//            int price = Integer.parseInt(mCartItemList.get(i).getProductItem().getmProductPriceString());
//            totalPrice[0] = totalPrice[0] + price;
//            Log.d("PROD LIST: ", ""+totalPrice[0]);
//        }

        //Hacky Fix //Bad Code ... I am not proud of this but deadline approaching
        PapersAdapter papersAdapter = new PapersAdapter(mContext, mCartItemList);
        mRecyclerView.setAdapter(papersAdapter);
    }


    public class PapersAdapter extends RecyclerView.Adapter<PapersAdapter.ViewHolder> {
        private final LayoutInflater inflater;
        private List<CartItem> cartItemList = new ArrayList<>();
        private Context mContext;

        int[] total = {0};


        public PapersAdapter(Context context, List<CartItem> cartItemList) {
            inflater = LayoutInflater.from(context);
            this.cartItemList.addAll(cartItemList);
            this.mContext = context;
            total = totalPrice;

        }

        public PapersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
            View root = inflater.inflate(R.layout.cart_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(root);
            return viewHolder;
        }

        public void onBindViewHolder(final ViewHolder viewHolder, int position) {
            final CartItem object = cartItemList.get(position);


            Utils.loadImage(viewHolder.imageView, object.getProductItem().getmImageUrl(), mContext);
            viewHolder.libItemName.setText(object.getProductItem().getmItemName());
            viewHolder.libItemSubject.setText(object.getProductItem().getmProductPriceString());


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //viewHolder.shipItemSwitch.toggle();
                }
            }, 1000);


            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductDetailsActivity.start(viewHolder.imageView.getContext(), object.getProductItem());
                }
            });

            mTotal.setText("N" + total[0]);
            mSubtotal.setText("N" + total[0]);

            viewHolder.shipItemSwitch.setOnCheckedChangeListener(new MaterialAnimatedSwitch.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(boolean b) {
                    if (b) {
                        Log.d("BEFORE ON: ", "" + total[0]);
                        total[0] = total[0] + ((Integer.parseInt(object.getProductItem().getmProductPriceString()) / 2) * Integer.parseInt(viewHolder.mQtyEditText.getText().toString()));
                        mSubtotal.setText("N" + total[0]);
                        mTotal.setText("N" + total[0]);
                    } else {
                        Log.d("BEFORE OF: ", "" + total[0]);
                        total[0] = total[0] - ((Integer.parseInt(object.getProductItem().getmProductPriceString()) / 2) * Integer.parseInt(viewHolder.mQtyEditText.getText().toString()));
                        mTotal.setText("N" + total[0]);
                        mSubtotal.setText("N" + total[0]);
                    }
                }
            });

            viewHolder.mQtyEditText.setText(String.valueOf(object.getQtyToBuy()));

            viewHolder.mQtyEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (viewHolder.shipItemSwitch.isChecked()) {
                            total[0] = total[0] + Integer.parseInt(object.getProductItem().getmProductPriceString()) * Integer.parseInt(v.getText().toString());
                            mTotal.setText("N" + total[0]);
                            mSubtotal.setText("N" + total[0]);

                            //Dismiss the mofuckin keyboard bish
                            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                            return true;
                        }
                    }
                    return false;
                }
            });

        }


        public final class ViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView libItemName;
            protected TextView libItemSubject;
            protected EditText mQtyEditText;
            protected MaterialAnimatedSwitch shipItemSwitch;
            protected TextView libItemDiscipline;

            public ViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.paper_icon);
                this.libItemName = (TextView) view.findViewById(R.id.paper_title);
                this.libItemDiscipline = (TextView) view.findViewById(R.id.paper_discipline);
                this.shipItemSwitch = (MaterialAnimatedSwitch) view.findViewById(R.id.pin);
                this.libItemSubject = (TextView) view.findViewById(R.id.paper_subject);
                this.mQtyEditText = (EditText) view.findViewById(R.id.qty_et);
            }
        }

        public int getItemCount() {
            return cartItemList.size();
        }
    }
}
