package com.alababic.pick2me.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.alababic.pick2me.utils.DialogUtils;
import com.alababic.pick2me.utils.SharedHelper;
import com.alababic.pick2me.utils.ToastUtils;

/**
 * Created by abdulmujibaliu on 3/8/17.
 */

public class BaseFragment extends Fragment {

    public SharedHelper mSharedHelperInstance;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedHelperInstance = BaseApplication.getInstance().getmAppSharedPrefHelper();

    }


    protected void handleSuccess(String response) {
        DialogUtils.hideDialog();
        ToastUtils.shortToast(response);
    }

    protected void handleFail(Throwable throwable) {
        DialogUtils.hideDialog();
        throwable.printStackTrace();
        ToastUtils.shortToast(throwable.getMessage());
    }


}

