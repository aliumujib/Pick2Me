package com.alababic.pick2me.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.alababic.pick2me.R;
import com.alababic.pick2me.utils.SharedHelper;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.utils.commons.Constants;

/**
 * Created by abdulmujibaliu on 3/8/17.
 */

public class BaseActivity extends AppCompatActivity {

    public SharedHelper mSharedHelperInstance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedHelperInstance = BaseApplication.getInstance().getmAppSharedPrefHelper();
    }



    public void replaceCurrentFragmentWithoutBackState(Fragment fragment) {
        Utils.replaceFragmentWithorWithoutBackState(fragment, this, Constants.WITHOUTBSTATE, R.id.frame_container_main);
    }

    public void replaceCurrentFragmentWithBackState(Fragment fragment) {
        Utils.replaceFragmentWithorWithoutBackState(fragment, this, Constants.WITHBSTATE, R.id.frame_container_main);
    }


}
