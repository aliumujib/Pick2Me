package com.alababic.pick2me.base;

import android.support.multidex.MultiDexApplication;

import com.alababic.pick2me.utils.SharedHelper;
import com.crashlytics.android.Crashlytics;

import fr.xebia.android.freezer.Freezer;
import io.fabric.sdk.android.Fabric;

public class BaseApplication extends MultiDexApplication {

    private static BaseApplication instance;
    private SharedHelper mAppSharedPrefHelper;

    public static BaseApplication getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Freezer.onCreate(this);
        instance = this;
    }



    public synchronized SharedHelper getmAppSharedPrefHelper() {
        return mAppSharedPrefHelper == null
                ? mAppSharedPrefHelper = new SharedHelper(this)
                : mAppSharedPrefHelper;
    }


    public BaseApplication() {
        super();
    }
}