/*
 * Copyright (c) 2017. Created by Aliu Abdul-Mujib for NugiTech on 2017, file last modified on 4/5/17 4:01 PM
 */

package com.alababic.pick2me.farmersfarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.import2me.Import2MeActivity;
import com.alababic_app.pick2me_data.models.TransactionItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FamersFarmActivity extends AppCompatActivity {

    ActionBar mActionBar;
    private ArrayList<TransactionItem> mTransactionItemArrayList = new ArrayList<>();
    private RecyclerView mTransactionRecyclerView;
    private TransactionsRecyclerViewAdapter mTransactionsListRVAdapter;
    private View mNoDataLayout;

    public static void start(Context context) {
        Intent intent = new Intent(context, FamersFarmActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmers_farm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if ((mActionBar = getSupportActionBar()) != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        mTransactionRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mTransactionRecyclerView.setLayoutManager(llm);
        mTransactionsListRVAdapter = new TransactionsRecyclerViewAdapter(this, mTransactionItemArrayList);
        mTransactionRecyclerView.setAdapter(mTransactionsListRVAdapter);
        mNoDataLayout = findViewById(R.id.no_content_view);

        if (mTransactionItemArrayList.size() == 0) {
            mNoDataLayout.setVisibility(View.VISIBLE);
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getItems();
            }
        }, 2000);
    }

    private void getItems() {
        mTransactionItemArrayList.add(new TransactionItem("Farm Admin", "Right weather to plant crops", "", "Beginning gardeners often worry about when to plant the plants they buy, yet the answer is simple: pretty much any time you want to.\n" +
                "\n" +
                "You can plant almost any time the ground isn’t frozen. That said, the two most popular seasons for planting are spring and fall (in the latter case, until about 2 weeks before the ground freezes for the winter), while the plants are more or less dormant… and when cooler temperatures mean digging holes is not as arduous for you.", "123445", new Date(1995, 3, 3)));
        mTransactionItemArrayList.add(new TransactionItem("Nugitech", "Right crops to plant in Lagos  Nigeria", "", "Beginning gardeners often worry about when to plant the plants they buy, yet the answer is simple: pretty much any time you want to.\n" +
                "\n" +
                "You can plant almost any time the ground isn’t frozen. That said, the two most popular seasons for planting are spring and fall (in the latter case, until about 2 weeks before the ground freezes for the winter), while the plants are more or less dormant… and when cooler temperatures mean digging holes is not as arduous for you.", "123445", new Date(1995, 3, 3)));
        mTransactionItemArrayList.add(new TransactionItem("Modro123", "How to get funds for farm app", "", "Beginning gardeners often worry about when to plant the plants they buy, yet the answer is simple: pretty much any time you want to.\n" +
                "\n" +
                "You can plant almost any time the ground isn’t frozen. That said, the two most popular seasons for planting are spring and fall (in the latter case, until about 2 weeks before the ground freezes for the winter), while the plants are more or less dormant… and when cooler temperatures mean digging holes is not as arduous for you.", "123445", new Date(1995, 3, 3)));
        mTransactionItemArrayList.add(new TransactionItem("NewseFarmer", "Rainfall Patterns to guide your planting", "", "Beginning gardeners often worry about when to plant the plants they buy, yet the answer is simple: pretty much any time you want to.\n" +
                "\n" +
                "You can plant almost any time the ground isn’t frozen. That said, the two most popular seasons for planting are spring and fall (in the latter case, until about 2 weeks before the ground freezes for the winter), while the plants are more or less dormant… and when cooler temperatures mean digging holes is not as arduous for you.", "123445", new Date(1995, 3, 3)));

        mNoDataLayout.setVisibility(View.GONE);
        mTransactionsListRVAdapter.notifyDataSetChanged();
    }


    class TransactionsRecyclerViewAdapter
            extends RecyclerView.Adapter<TransactionsRecyclerViewAdapter.ViewHolder> {

        private final String TAG = getClass().getSimpleName();
        private final List<TransactionItem> mValues;
        private Context mContext;

        public TransactionsRecyclerViewAdapter(Context context, List<TransactionItem> items) {
            mValues = items;
            mContext = context;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.transaction_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final TransactionItem transactionItem = mValues.get(position);
            holder.mTransactionDesc.setText(Html.fromHtml(transactionItem.getmPaymentMethod()));
            holder.mTimeStampTextView.setText(transactionItem.getmAmount());
            holder.mCardNumber.setText(transactionItem.getmPINCode());
            holder.mTransactionID.setText(transactionItem.getmObjID());
            holder.mCardNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Import2MeActivity.start(holder.mCardNumber.getContext());
                }
            });

        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView mTransactionDesc, mCardNumber, mTimeStampTextView, mTransactionID;

            ViewHolder(View itemView) {
                super(itemView);
                mTransactionDesc = (TextView) itemView.findViewById(R.id.transaction_desc);
                mTimeStampTextView = (TextView) itemView.findViewById(R.id.date_of_purchase);
                mCardNumber = (TextView) itemView.findViewById(R.id.card_pin_number);
                mTransactionID = (TextView) itemView.findViewById(R.id.transaction_id);
            }

        }
    }

}

