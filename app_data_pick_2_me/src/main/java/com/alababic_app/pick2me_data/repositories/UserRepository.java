package com.alababic_app.pick2me_data.repositories;

import com.alababic_app.pick2me_data.contracts.IUserRepository;
import com.alababic_app.pick2me_data.models.User;
import com.alababic_app.pick2me_data.models.UserEntityManager;

import java.util.List;

import fr.xebia.android.freezer.async.Callback;
import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;

/**
 * Created by ABDUL-MUJEEB ALIU on 27/11/2017.
 */

public class UserRepository implements IUserRepository {

    private UserEntityManager mUserEntityManager = new UserEntityManager();

    public static IUserRepository sharedInstance = new UserRepository();

    @Override
    public Observable<Notification<User>> getCurrentUser() {
        return Observable.create(new ObservableOnSubscribe<Notification<User>>() {
            @Override
            public void subscribe(final ObservableEmitter<Notification<User>> emitter) throws Exception {
                mUserEntityManager.select().async(new Callback<List<User>>() {
                    @Override
                    public void onSuccess(List<User> data) {
                        if (data.isEmpty()) {
                            emitter.onNext(Notification.<User>createOnError(new Throwable("User is not signed in")));
                        } else {
                            emitter.onNext(Notification.createOnNext(data.get(0)));
                        }
                    }

                    @Override
                    public void onError(List<User> data) {
                        emitter.onNext(Notification.<User>createOnError(new Throwable("User is not signed in")));
                        mUserEntityManager.deleteAll();
                    }
                });
            }
        });
    }

    @Override
    public Observable<Boolean> isUserSignedIn() {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(final ObservableEmitter<Boolean> observableEmitter) throws Exception {
                getCurrentUser().subscribe(new Consumer<Notification<User>>() {
                    @Override
                    public void accept(Notification<User> userNotification) throws Exception {
                        if (userNotification.isOnError()) {
                            observableEmitter.onNext(false);
                        } else {
                            observableEmitter.onNext(true);
                        }
                    }
                });
            }
        });
    }

    @Override
    public Observable<Notification<Boolean>> saveCurrentUser(final User user) {
        return Observable.create(new ObservableOnSubscribe<Notification<Boolean>>() {
            @Override
            public void subscribe(final ObservableEmitter<Notification<Boolean>> emitter) throws Exception {
                mUserEntityManager.deleteAll();
                mUserEntityManager.addAsync(user, new Callback<User>() {
                    @Override
                    public void onSuccess(User data) {
                        emitter.onNext(Notification.createOnNext(true));
                    }

                    @Override
                    public void onError(User data) {
                        emitter.onNext(Notification.createOnNext(false));
                    }
                });
            }
        });
    }

    @Override
    public Observable<Notification<Boolean>> updateCurrentUserAuthToken(String authToken) {
        return null;
    }

}
