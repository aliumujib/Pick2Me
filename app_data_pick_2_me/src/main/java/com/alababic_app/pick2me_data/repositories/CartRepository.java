package com.alababic_app.pick2me_data.repositories;

import android.util.Log;

import com.alababic_app.pick2me_data.contracts.ICartRepository;
import com.alababic_app.pick2me_data.models.CartItem;
import com.alababic_app.pick2me_data.models.CartItemEntityManager;
import com.alababic_app.pick2me_data.models.ProductItem;
import com.alababic_app.pick2me_data.models.ProductItemQueryBuilder;

import java.util.List;
import java.util.Random;

import fr.xebia.android.freezer.async.Callback;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.ReplaySubject;

/**
 * Created by abdulmujibaliu on 11/13/17.
 */

public class CartRepository implements ICartRepository {

    ReplaySubject<Integer> mCartCountIntegerReplaySubject = ReplaySubject.create();
    private CartItemEntityManager cartItemEntityManager = new CartItemEntityManager();
    private String TAG = getClass().getSimpleName();
    private int cartCountInt = 0;

    public static ICartRepository sharedInstance = new CartRepository();


    public CartRepository() {
        initCartCount();
    }

    @Override
    public void addItemToCart(final ProductItem productItem) {

        getIsItemInCart(productItem).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean isInCart) throws Exception {
                if (!isInCart) {
                    incrementCount();
                    Random random = new Random();
                    CartItem cartI = new CartItem(random.nextLong(), productItem, 1);
                    cartItemEntityManager.add(cartI);
                    // ToastUtils.shortToast("Added Item to cart!");
                } else {
                    incrementCartItemQty(productItem);
                }
            }
        });
    }

    private void incrementCartItemQty(ProductItem productItem) {
        cartItemEntityManager.select().productItem(new ProductItemQueryBuilder().mItemName().equalsTo(productItem.getmItemName())).async(new Callback<List<CartItem>>() {
            @Override
            public void onSuccess(List<CartItem> data) {
                if (!data.isEmpty()) {
                    CartItem cartItem = data.get(0);
                    cartItem.setQtyToBuy(data.get(0).getQtyToBuy() + 1);
                    cartItemEntityManager.update(cartItem);
                } else {
                    Log.d(TAG, "EMpty prodcut item");
                }
            }

            @Override
            public void onError(List<CartItem> data) {

            }
        });
    }

    private Observable<Boolean> getIsItemInCart(final ProductItem productItem) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(final ObservableEmitter<Boolean> observableOnSubscribe) throws Exception {
                cartItemEntityManager.select().productItem(new ProductItemQueryBuilder().mItemName().equalsTo(productItem.getmItemName())).async(new Callback<List<CartItem>>() {
                    @Override
                    public void onSuccess(List<CartItem> data) {
                        if (data.isEmpty()) {
                            observableOnSubscribe.onNext(false);
                        } else {
                            observableOnSubscribe.onNext(true);
                        }
                    }

                    @Override
                    public void onError(List<CartItem> data) {
                        if (data.isEmpty()) {
                            observableOnSubscribe.onNext(false);
                        } else {
                            observableOnSubscribe.onNext(true);
                        }
                    }
                });
            }
        });
    }

    private void initCartCount() {
        cartItemEntityManager.select().async(new Callback<List<CartItem>>() {
            @Override
            public void onSuccess(List<CartItem> data) {
                cartCountInt = data.size();
                emitCartCount();
            }

            @Override
            public void onError(List<CartItem> data) {
                cartCountInt = data.size();
                emitCartCount();
            }
        });
    }

    private void incrementCount() {
        cartCountInt = cartCountInt + 1;
        emitCartCount();
    }

    private void emitCartCount() {
        mCartCountIntegerReplaySubject.onNext(cartCountInt);
    }

    private void decrementCount() {
        cartCountInt = cartCountInt + 1;
        emitCartCount();
    }

    @Override
    public Observable<Integer> getCartCount() {
        return mCartCountIntegerReplaySubject;
    }

    @Override
    public void removeItemFromCart(ProductItem productItem) {

    }

    @Override
    public Observable<List<CartItem>> getCartItems() {
        return null;
    }
}
