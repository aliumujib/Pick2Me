package com.alababic_app.pick2me_data.repositories;

import com.alababic_app.pick2me_data.R;
import com.alababic_app.pick2me_data.contracts.IRepoContracts;
import com.alababic_app.pick2me_data.models.Category;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by abdulmujibaliu on 10/22/17.
 */

public class CategoriesRepository implements IRepoContracts.ICategoriesRepository {

    public static IRepoContracts.ICategoriesRepository sharedInstance = new CategoriesRepository();

    @Override
    public Observable<List<Category>> getCategoryData() {
        return Observable.just(getRecources());
    }

    private List<Category> getRecources() {

        /**
         *     <color name="amber">#FFC107</color>
         <color name="teal">#009688</color>
         <color name="red">#D32F2F</color>
         <color name="lime">#AFB42B</color>
         <color name="indigo_500">#3F51B5</color>
         <color name="indigo_700">#303F9F</color>
         <color name="yellow_500">#FFEB3B</color>
         <color name="orange">#F57C00</color>
         * */

        ArrayList<Category> categoryArrayList = new ArrayList<>();
        categoryArrayList.add(new Category(R.color.amber, "Restaurant", "", R.drawable.restaurant));
        categoryArrayList.add(new Category(R.color.red, "Supermarket", "", R.drawable.supermarket));
        categoryArrayList.add(new Category(R.color.lime, "Pharmacy", "", R.drawable.pharmarcy));
        categoryArrayList.add(new Category(R.color.indigo_500, "Personal", "", R.drawable.personal));
        categoryArrayList.add(new Category(R.color.yellow_500, "Seller and e-Commerce", "", R.drawable.ecommerce));
        categoryArrayList.add(new Category(R.color.orange, "City to City", "", R.drawable.citytocity));
        categoryArrayList.add(new Category(R.color.colorAccent, "Buy 2 me", "", R.drawable.buy2me));
        categoryArrayList.add(new Category(R.color.green, "Ship and Shop", "", R.drawable.shopandship));
        categoryArrayList.add(new Category(R.color.green_teal, "Import", "", R.drawable.import2me));
        categoryArrayList.add(new Category(R.color.teal, "Offers", "", R.drawable.offfers));

        return categoryArrayList;
    }

}
