package com.alababic_app.pick2me_data.contracts;

import com.alababic_app.pick2me_data.models.Buy2MeItem;
import com.alababic_app.pick2me_data.models.ProductItem;

import java.util.List;

import io.reactivex.Notification;
import io.reactivex.Observable;

/**
 * Created by ABDUL-MUJEEB ALIU on 25/11/2017.
 */

public interface IBuy2MeRepository {

    Observable<List<Buy2MeItem>> getBuy2MeListData();

    Observable<Notification<Boolean>> saveBuy2Me();

}
