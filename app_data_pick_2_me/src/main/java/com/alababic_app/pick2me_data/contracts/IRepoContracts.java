package com.alababic_app.pick2me_data.contracts;

import com.alababic_app.pick2me_data.models.Category;
import com.alababic_app.pick2me_data.models.ProductItem;
import com.alababic_app.pick2me_data.models.Shop;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by abdulmujibaliu on 10/22/17.
 */

public interface IRepoContracts {

    interface ICategoriesRepository {
        Observable<List<Category>> getCategoryData();

    }

    interface IProductsRepository {
        Observable<List<ProductItem>> getProductData();
    }

    interface IShopsRepository {
        Observable<List<Shop>> getShopData();
    }

}
