package com.alababic_app.pick2me_data.contracts;

import android.content.Context;

import com.alababic_app.pick2me_data.models.PasswordResetData;
import com.alababic_app.pick2me_data.models.User;

import io.reactivex.Observable;

/**
 * Created by abdulmujibaliu on 11/11/17.
 */

public interface IAuthContracts {

    interface ILoginRepository {

        Observable<User> signUserIn(Context context, User user);
    }

    interface ISignUpRepository {
        Observable<User> signUserUp(Context context, User user);
    }

    interface IForgotPasswordRepository {
        Observable<Boolean> resetUserPassword(Context context, PasswordResetData email);
    }


}
