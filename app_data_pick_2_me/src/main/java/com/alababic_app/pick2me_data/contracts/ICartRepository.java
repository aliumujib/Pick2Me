package com.alababic_app.pick2me_data.contracts;

import com.alababic_app.pick2me_data.models.CartItem;
import com.alababic_app.pick2me_data.models.ProductItem;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by abdulmujibaliu on 11/13/17.
 */

public interface ICartRepository {

    void addItemToCart(ProductItem productItem);

    Observable<Integer> getCartCount();

    void removeItemFromCart(ProductItem productItem);


    Observable<List<CartItem>> getCartItems();
}
