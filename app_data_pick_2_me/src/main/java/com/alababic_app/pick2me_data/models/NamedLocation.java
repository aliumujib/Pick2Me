package com.alababic_app.pick2me_data.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by abdulmujibaliu on 9/18/17.
 */

public class NamedLocation {

    public final String name;

    public final LatLng location;

    public NamedLocation(String name, LatLng location) {
        this.name = name;
        this.location = location;
    }
}