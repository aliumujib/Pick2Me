package com.alababic_app.pick2me_data.models;

/**
 * Created by abdulmujibaliu on 11/11/17.
 */

public class LoginData {
    String email;
    String password;

    public LoginData(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
