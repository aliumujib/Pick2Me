package com.alababic_app.pick2me_data.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abdulmujibaliu on 3/7/17.
 */

public class JournalItem implements Parcelable {

    public String mLanguage, mISSN, mEstablishmentYear, mPublishedArticleCount, mTitle, mImageURL,
            mExecutiveEditor, mPublisher, mSubmissionEmail, mAboutUS, mSubGuideLines, mEditorialBoard, mDBID;
    public int mPublishingFrequency;


    public String getmImageURL() {
        return mImageURL;
    }

    public void setmImageURL(String mImageURL) {
        this.mImageURL = mImageURL;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmISSN() {
        return mISSN;
    }

    public void setmISSN(String mISSN) {
        this.mISSN = mISSN;
    }

    public String getmEstablishmentYear() {
        return mEstablishmentYear;
    }

    public void setmEstablishmentYear(String mEstablishmentYear) {
        this.mEstablishmentYear = mEstablishmentYear;
    }

    public String getmPublishedArticleCount() {
        return mPublishedArticleCount;
    }

    public void setmPublishedArticleCount(String mPublishedArticleCount) {
        this.mPublishedArticleCount = mPublishedArticleCount;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmExecutiveEditor() {
        return mExecutiveEditor;
    }

    public void setmExecutiveEditor(String mExecutiveEditor) {
        this.mExecutiveEditor = mExecutiveEditor;
    }

    public String getmPublisher() {
        return mPublisher;
    }

    public void setmPublisher(String mPublisher) {
        this.mPublisher = mPublisher;
    }

    public String getmSubmissionEmail() {
        return mSubmissionEmail;
    }

    public void setmSubmissionEmail(String mSubmissionEmail) {
        this.mSubmissionEmail = mSubmissionEmail;
    }

    public String getmAboutUS() {
        return mAboutUS;
    }

    public void setmAboutUS(String mAboutUS) {
        this.mAboutUS = mAboutUS;
    }

    public String getmSubGuideLines() {
        return mSubGuideLines;
    }

    public void setmSubGuideLines(String mSubGuideLines) {
        this.mSubGuideLines = mSubGuideLines;
    }

    public String getmEditorialBoard() {
        return mEditorialBoard;
    }

    public void setmEditorialBoard(String mEditorialBoard) {
        this.mEditorialBoard = mEditorialBoard;
    }

    public String getmDBID() {
        return mDBID;
    }

    public void setmDBID(String mDBID) {
        this.mDBID = mDBID;
    }

    public int getmPublishingFrequency() {
        return mPublishingFrequency;
    }

    public void setmPublishingFrequency(int mPublishingFrequency) {
        this.mPublishingFrequency = mPublishingFrequency;
    }

    public static Creator<JournalItem> getCREATOR() {
        return CREATOR;
    }



    public JournalItem(String mImageURL, String mLanguage, String mISSN, String mEstablishmentYear, String mPublishedArticleCount,
                       String mTitle, String mExecutiveEditor, String mPublisher, String mSubmissionEmail,
                       String mAboutUS, String mSubGuideLines, String mEditorialBoard, String mDBID, int mPublishingFrequency) {
        this.mImageURL = mImageURL;
        this.mLanguage = mLanguage;
        this.mISSN = mISSN;
        this.mEstablishmentYear = mEstablishmentYear;
        this.mPublishedArticleCount = mPublishedArticleCount;
        this.mTitle = mTitle;
        this.mExecutiveEditor = mExecutiveEditor;
        this.mPublisher = mPublisher;
        this.mSubmissionEmail = mSubmissionEmail;
        this.mAboutUS = mAboutUS;
        this.mSubGuideLines = mSubGuideLines;
        this.mEditorialBoard = mEditorialBoard;
        this.mDBID = mDBID;
        this.mPublishingFrequency = mPublishingFrequency;
    }

    public JournalItem() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mLanguage);
        dest.writeString(this.mISSN);
        dest.writeString(this.mEstablishmentYear);
        dest.writeString(this.mPublishedArticleCount);
        dest.writeString(this.mTitle);
        dest.writeString(this.mImageURL);
        dest.writeString(this.mExecutiveEditor);
        dest.writeString(this.mPublisher);
        dest.writeString(this.mSubmissionEmail);
        dest.writeString(this.mAboutUS);
        dest.writeString(this.mSubGuideLines);
        dest.writeString(this.mEditorialBoard);
        dest.writeString(this.mDBID);
        dest.writeInt(this.mPublishingFrequency);
    }

    protected JournalItem(Parcel in) {
        this.mLanguage = in.readString();
        this.mISSN = in.readString();
        this.mEstablishmentYear = in.readString();
        this.mPublishedArticleCount = in.readString();
        this.mTitle = in.readString();
        this.mImageURL = in.readString();
        this.mExecutiveEditor = in.readString();
        this.mPublisher = in.readString();
        this.mSubmissionEmail = in.readString();
        this.mAboutUS = in.readString();
        this.mSubGuideLines = in.readString();
        this.mEditorialBoard = in.readString();
        this.mDBID = in.readString();
        this.mPublishingFrequency = in.readInt();
    }

    public static final Creator<JournalItem> CREATOR = new Creator<JournalItem>() {
        @Override
        public JournalItem createFromParcel(Parcel source) {
            return new JournalItem(source);
        }

        @Override
        public JournalItem[] newArray(int size) {
            return new JournalItem[size];
        }
    };
}
