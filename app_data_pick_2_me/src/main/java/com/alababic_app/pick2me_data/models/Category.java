package com.alababic_app.pick2me_data.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Abdul-Mujeeb Aliu on 12/5/2015.
 */
public class Category implements Parcelable {


    String lectureID;
    int recourceBgColorID, recoursePicID; // 1: Word 2:PDF 3:Excel 4:PowerPoint
    String categName;


    public Category(int recourceBgColorID, String categName, String lectureID) {
        this.recourceBgColorID = recourceBgColorID;
        this.categName = categName;
        this.lectureID = lectureID;
    }

    public int getRecoursePicID() {
        return recoursePicID;
    }

    public void setRecoursePicID(int recoursePicID) {
        this.recoursePicID = recoursePicID;
    }

    public Category(int recourceBgColorID, String categName, String lectureID , int recoursePicID) {
        this.lectureID = lectureID;
        this.recourceBgColorID = recourceBgColorID;
        this.recoursePicID = recoursePicID;
        this.categName = categName;
    }

    public int getRecourceBgColorID() {
        return recourceBgColorID;
    }

    public void setRecourceBgColorID(int recourceBgColorID) {
        this.recourceBgColorID = recourceBgColorID;
    }

    public String getCategName() {
        return categName;
    }

    public void setCategName(String categName) {
        this.categName = categName;
    }

    public String getLectureID() {
        return lectureID;
    }

    public void setLectureID(String lectureID) {
        this.lectureID = lectureID;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.lectureID);
        dest.writeInt(this.recourceBgColorID);
        dest.writeInt(this.recoursePicID);
        dest.writeString(this.categName);
    }

    protected Category(Parcel in) {
        this.lectureID = in.readString();
        this.recourceBgColorID = in.readInt();
        this.recoursePicID = in.readInt();
        this.categName = in.readString();
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
