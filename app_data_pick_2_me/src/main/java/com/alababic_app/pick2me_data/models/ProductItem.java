package com.alababic_app.pick2me_data.models;

import android.os.Parcel;
import android.os.Parcelable;

import fr.xebia.android.freezer.annotations.Model;

/**
 * Created by aliumujib on 04/04/16.
 */

@Model
public class ProductItem implements Parcelable {


    public String mItemName;
    public String mImageUrl;
    public String mSellerName;
    public String mProductCategory;
    public String mProductType;
    public String mProductProducer;
    public String mProductDateOfExpiryYear;
    public String mProductMinimumQty;
    public String mProductPriceString;
    public String mProductDescription;

    public int mProductCategoryType = 0;

    public int getmProductCategoryType() {
        return mProductCategoryType;
    }

    public void setmProductCategoryType(int mProductCategoryType) {
        this.mProductCategoryType = mProductCategoryType;
    }

    public String getmProductDescription() {
        return mProductDescription;
    }

    public void setmProductDescription(String mProductDescription) {
        this.mProductDescription = mProductDescription;
    }

    public String getmProductPriceString() {
        return mProductPriceString;
    }

    public void setmProductPriceString(String mProductPriceString) {
        this.mProductPriceString = mProductPriceString;
    }

    public ProductItem() {
    }

    public ProductItem(String mImageUrl, String mItemName, String mSellerName,
                       String mProductCategory, String mProductType, String mProductProducer,
                       String mProductDateOfExpiryYear, String mProductMinimumQty, String mProductPriceString, String mProductDescription) {
        this.mImageUrl = mImageUrl;
        this.mItemName = mItemName;
        this.mSellerName = mSellerName;
        this.mProductCategory = mProductCategory;
        this.mProductType = mProductType;
        this.mProductProducer = mProductProducer;
        this.mProductDateOfExpiryYear = mProductDateOfExpiryYear;
        this.mProductMinimumQty = mProductMinimumQty;
        this.mProductPriceString = mProductPriceString;
        this.mProductDescription = mProductDescription;
    }

    public ProductItem(String mImageUrl, String mItemName, String mSellerName) {
        this.mImageUrl = mImageUrl;
        this.mItemName = mItemName;
        this.mSellerName = mSellerName;
    }


    public void setmItemName(String mItemName) {
        this.mItemName = mItemName;
    }

    public void setmSellerName(String mSellerName) {
        this.mSellerName = mSellerName;
    }

    public String getmProductProducer() {
        return mProductProducer;
    }

    public void setmProductProducer(String mProductProducer) {
        this.mProductProducer = mProductProducer;
    }

    public String getmProductDateOfExpiryYear() {
        return mProductDateOfExpiryYear;
    }

    public void setmProductDateOfExpiryYear(String mProductDateOfExpiryYear) {
        this.mProductDateOfExpiryYear = mProductDateOfExpiryYear;
    }

    public String getmProductMinimumQty() {
        return mProductMinimumQty;
    }

    public void setmProductMinimumQty(String mProductMinimumQty) {
        this.mProductMinimumQty = mProductMinimumQty;
    }

    public void setItemName(String item_name) {
        this.mItemName = item_name;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setSellerName(String seller_name) {
        this.mSellerName = seller_name;
    }

    public void setmProductCategory(String mProductCategory) {
        this.mProductCategory = mProductCategory;
    }

    public void setmProductType(String mProductType) {
        this.mProductType = mProductType;
    }

    public String getmProductType() {
        return mProductType;
    }

    public String getmItemName() {
        return mItemName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public String getmSellerName() {
        return mSellerName;
    }

    public String getmProductCategory() {
        return mProductCategory;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mItemName);
        dest.writeString(this.mImageUrl);
        dest.writeString(this.mSellerName);
        dest.writeString(this.mProductCategory);
        dest.writeString(this.mProductType);
        dest.writeString(this.mProductProducer);
        dest.writeString(this.mProductDateOfExpiryYear);
        dest.writeString(this.mProductMinimumQty);
        dest.writeString(this.mProductPriceString);
        dest.writeString(this.mProductDescription);
        dest.writeInt(this.mProductCategoryType);
    }

    protected ProductItem(Parcel in) {
        this.mItemName = in.readString();
        this.mImageUrl = in.readString();
        this.mSellerName = in.readString();
        this.mProductCategory = in.readString();
        this.mProductType = in.readString();
        this.mProductProducer = in.readString();
        this.mProductDateOfExpiryYear = in.readString();
        this.mProductMinimumQty = in.readString();
        this.mProductPriceString = in.readString();
        this.mProductDescription = in.readString();
        this.mProductCategoryType = in.readInt();
    }

    public static final Creator<ProductItem> CREATOR = new Creator<ProductItem>() {
        @Override
        public ProductItem createFromParcel(Parcel source) {
            return new ProductItem(source);
        }

        @Override
        public ProductItem[] newArray(int size) {
            return new ProductItem[size];
        }
    };
}

